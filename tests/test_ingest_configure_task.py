import datetime
from unittest import mock
from uuid import uuid4

import pytest

from flywheel_cli import errors as global_errors
from flywheel_cli.ingest import config, errors
from flywheel_cli.ingest import models as M
from flywheel_cli.ingest import schemas as T
from flywheel_cli.ingest.client import db_transactions
from flywheel_cli.ingest.client.db import BatchWriter, DBClient
from flywheel_cli.ingest.tasks import configure


@pytest.fixture(scope="function")
def configure_task():
    task = T.TaskOut(
        type="configure",
        id=uuid4(),
        ingest_id=uuid4(),
        status="pending",
        timestamp=0,
        retries=0,
        history=[],
        created=datetime.datetime.now(),
        context={"scanner": {"type": "filename", "dir": "/tmp", "opts": {}}},
    )
    db_spec = dir(DBClient)
    db_spec.extend(["batch_writer_insert_error", "update_task"])
    mock.Mock(spec=db_spec)

    configure_task = configure.ConfigureTask(
        db=mock.Mock(spec=db_spec),
        task=task,
        worker_config=config.WorkerConfig(),
        is_local=True,
    )

    configure_task.ingest_config = config.IngestConfig(src_fs="/tmp")

    return configure_task


@pytest.fixture(scope="function")
def create_configure_task(
    db, sdk_mock, test_data_dir
):  # pylint: disable=unused-argument
    def _create(task_kwargs=None, ingest_kwargs=None):
        ingest_kwargs = ingest_kwargs or {}
        ingest_kwargs.setdefault("status", "configuring")
        ingest_kwargs.setdefault("config", {"src_fs": test_data_dir})
        db.create_ingest(**ingest_kwargs)

        task_kwargs = task_kwargs or {}
        task_kwargs.setdefault("type", "configure")
        task_kwargs.setdefault("status", "running")
        task_kwargs.setdefault(
            "context", {"scanner": {"type": "filename", "dir": "/tmp", "opts": {}}}
        )
        task = db.create_task(**task_kwargs)
        task = T.TaskOut.from_orm(task)

        worker_config = config.WorkerConfig()

        return configure.ConfigureTask(
            db=db.client, task=task, worker_config=worker_config, is_local=True
        )

    return _create


def test_verify_permissions_ok(
    configure_task, sdk_mock
):  # pylint: disable=unused-argument
    configure_task.scanner_type = "template"
    configure_task._initialize()  # pylint: disable=protected-access
    configure_task.verify_permissions()


def test_verify_permissions_cant_import(configure_task, sdk_mock):
    sdk_mock.can_import_into.side_effect = global_errors.NotEnoughPermissions()
    configure_task.scanner_type = "template"

    configure_task._initialize()  # pylint: disable=protected-access
    with pytest.raises(errors.NotEnoughPermissions):
        configure_task.verify_permissions()


def test_verify_permissions_cant_create(configure_task, sdk_mock):
    sdk_mock.can_create_project_in_group.side_effect = (
        global_errors.NotEnoughPermissions()
    )
    configure_task.scanner_type = "template"
    configure_task.ingest_config.copy_duplicates = True

    configure_task._initialize()  # pylint: disable=protected-access
    with pytest.raises(errors.NotEnoughPermissions):
        configure_task.verify_permissions()


def test_resolve_detect_duplicates_projects(configure_task, sdk_mock, attr_dict):
    def lookup(path):
        if path == ["grp1", "prj1"]:
            return attr_dict({"container_type": "project", "id": "id1"})
        return None

    sdk_mock.safe_lookup.side_effect = lookup
    configure_task.ingest_config.detect_duplicates_project = [
        "grp1/prj1",
        "badpath",
        "grp1/prj1",
    ]
    configure_task.insert_errors = mock.Mock(spec=BatchWriter)

    configure_task._initialize()  # pylint: disable=protected-access
    configure_task.resolve_detect_duplicates_projects()

    assert configure_task.insert_errors.push.call_count == 1
    configure_task.insert_errors.push.assert_called_once_with(
        {
            "task_id": configure_task.task.id,
            "code": errors.ProjectDoesNotExistError.code,
            "message": mock.ANY,
        }
    )

    configure_task.db.update_ingest_config.assert_called_once_with(
        configure_task.ingest_config
    )
    assert configure_task.ingest_config.detect_duplicates_project_ids == set(["id1"])


def test_resolve_detect_duplicates_projects_no_permission(
    configure_task, sdk_mock, attr_dict
):
    sdk_mock.safe_lookup.return_value = attr_dict(
        {"container_type": "project", "id": "id1"}
    )
    sdk_mock.get_user_actions.return_value = set()
    sdk_mock.auth_info = attr_dict({"is_admin": False})

    configure_task.ingest_config.detect_duplicates_project = [
        "grp1/prj1",
        "badpath",
        "grp1/prj1",
    ]
    configure_task.insert_errors = mock.Mock(spec=BatchWriter)

    configure_task._initialize()  # pylint: disable=protected-access
    with pytest.raises(errors.NotEnoughPermissions):
        configure_task.resolve_detect_duplicates_projects()


def test_fetch_deid_profile(configure_task, sdk_mock):
    configure_task.strategy_config = config.DicomConfig(group="gid", project="pid")

    sdk_mock.get_deid_profile.return_value = {"key": "value"}
    configure_task.fetch_deid_profile()

    configure_task.db.update_ingest_config.assert_called_once_with(
        configure_task.ingest_config
    )
    assert configure_task.ingest_config.de_identify
    assert configure_task.ingest_config.deid_profile == "server_deid_profile"
    assert configure_task.ingest_config.deid_profiles == [
        {"key": "value", "name": "server_deid_profile"}
    ]
    assert configure_task.ingest_config.deid_is_from_server


def test_fetch_deid_profile_no_project(
    configure_task, sdk_mock
):  # pylint: disable=unused-argument
    configure_task.strategy_config = config.TemplateConfig(template="")

    with pytest.raises(errors.StopIngestError):
        configure_task.fetch_deid_profile()

    configure_task.db.add.assert_called_once()


def test_run(configure_task, sdk_mock):
    sdk_mock.get_deid_profile.return_value = None

    configure_task._initialize()  # pylint: disable=protected-access
    configure_task.run()

    configure_task.db.update_task.assert_called_once_with(
        configure_task.task.id, status=T.TaskStatus.completed
    )
    configure_task.db.start_scanning.assert_called_once()


def test_fetch_deid_profile_local_conflict(configure_task, sdk_mock):
    configure_task.strategy_config = config.DicomConfig(group="gid", project="pid")
    configure_task.ingest_config.de_identify = True
    configure_task.ingest_config.deid_profile = "minimal"

    sdk_mock.get_deid_profile.return_value = {"key": "value"}
    with pytest.raises(errors.BaseIngestError):
        configure_task.fetch_deid_profile()


def test_run_fail(configure_task, sdk_mock):  # pylint: disable=unused-argument
    configure_task.strategy_config = config.DicomConfig(group="gid", project="pid")
    configure_task.ingest_config.de_identify = True
    configure_task.ingest_config.deid_profile = "minimal"

    configure_task._initialize()  # pylint: disable=protected-access
    configure_task.run()

    configure_task.db.update_task.assert_called_once_with(
        configure_task.task.id, status=T.TaskStatus.failed
    )
    configure_task.db.fail.assert_called_once()
    configure_task.db.start_scanning.assert_not_called()


def test_status_count_fail(db, create_configure_task):
    task = create_configure_task(task_kwargs={"status": "pending"})
    status_count = db.client.find_one_task_stat(M.TaskStat.type == "configure")
    assert status_count.pending == 0
    assert status_count.running == 0
    assert status_count.completed == 0
    assert status_count.failed == 0

    db_transactions.update_task_stat(
        db.session, task.ingest.id, "configure", pending=M.TaskStat.pending + 1
    )
    db.session.commit()
    w_task = db.client.next_task("worker")
    assert w_task.id == task.task.id
    status_count = db.client.find_one_task_stat(M.TaskStat.type == "configure")
    assert status_count.pending == 0
    assert status_count.running == 1

    task.run()

    status_count = db.client.find_one_task_stat(M.TaskStat.type == "configure")
    assert status_count.pending == 0
    assert status_count.running == 0
    assert status_count.completed == 0
    assert status_count.failed == 1


def test_status_count_success(db, create_configure_task, sdk_mock):
    sdk_mock.get_deid_profile.return_value = {"key": "value"}
    config = {
        "strategy_config": {"strategy_name": "dicom", "group": "gid", "project": "pid"}
    }
    task = create_configure_task(
        ingest_kwargs=config, task_kwargs={"status": "pending"}
    )
    status_count = db.client.find_one_task_stat(M.TaskStat.type == "configure")
    assert status_count.pending == 0
    assert status_count.running == 0
    assert status_count.completed == 0
    assert status_count.failed == 0

    db_transactions.update_task_stat(
        db.session, task.ingest.id, "configure", pending=M.TaskStat.pending + 1
    )
    db.session.commit()
    w_task = db.client.next_task("worker")
    assert w_task.id == task.task.id
    status_count = db.client.find_one_task_stat(M.TaskStat.type == "configure")
    assert status_count.pending == 0
    assert status_count.running == 1

    task.run()

    status_count = db.client.find_one_task_stat(M.TaskStat.type == "configure")
    assert status_count.pending == 0
    assert status_count.running == 0
    assert status_count.completed == 1
    assert status_count.failed == 0


def test_deid_log_conflict(configure_task, sdk_mock):
    configure_task.strategy_config = config.DicomConfig(group="gid", project="pid")
    sdk_mock.get_deid_profile.return_value = {"key": "value"}
    configure_task.fw.deid_log = True

    configure_task.fetch_deid_profile()

    sdk_mock.get_deid_profile.return_value = None
    with pytest.raises(errors.BaseIngestError):
        configure_task.fetch_deid_profile()


def test_deid_profile_not_enabled(configure_task, sdk_mock):
    configure_task.strategy_config = config.DicomConfig(group="gid", project="pid")

    sdk_mock.deid_profile = False
    sdk_mock.get_deid_profile.return_value = {"key": "value"}
    configure_task.fetch_deid_profile()

    configure_task.db.update_ingest_config.assert_not_called()
    assert not configure_task.ingest_config.de_identify
