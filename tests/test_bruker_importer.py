import collections
import os

import fs
import pytest

from flywheel_cli.config import Config
from flywheel_cli.importers.bruker_scan import create_bruker_scanner
from flywheel_cli.walker import PyFsWalker

from .test_container_factory import MockContainerResolver


@pytest.fixture(scope="session")
def bruker_file_content(test_data_dir):
    def get_bruker_file(filename, version=None):
        path = os.path.join(test_data_dir, "bruker", filename)
        with open(path, "r", encoding="utf-8") as fh:
            content = fh.read()

        if version:
            print("sdsdfsdf")
            return f"##TITLE=Parameter List, ParaVision {version}\n{content}"
        return content

    return get_bruker_file


@pytest.mark.parametrize(
    "ver, pvtype",
    [
        ("5.0.1", "pv5"),
        ("6.0.1", "pv6"),
    ],
)
def test_import(ver, pvtype, bruker_file_content):
    subject = bruker_file_content("subject.txt", ver).encode("utf-8")
    acqp = bruker_file_content("acqp.txt", ver).encode("utf-8")

    mockfs = mock_fs(
        collections.OrderedDict(
            {
                "/session": [("subject", subject)],
                "/session/acquisition": [("acqp", acqp)],
            }
        )
    )

    resolver = MockContainerResolver()
    importer = create_bruker_scanner("gid", "pid", make_config(resolver))
    walker = PyFsWalker("mockfs://", src_fs=mockfs)
    importer.discover(walker)

    itr = iter(importer.container_factory.walk_containers())

    _, child = next(itr)
    assert child.container_type == "group"
    assert child.id == "gid"

    _, child = next(itr)
    assert child.container_type == "project"
    assert child.label == "pid"
    assert child.files == []

    _, child = next(itr)
    assert child.container_type == "subject"
    assert child.label == "010005-m00"

    _, child = next(itr)
    assert child.container_type == "session"
    assert child.label == "protocol 76"

    _, child = next(itr)
    assert child.container_type == "acquisition"
    assert child.label == "acquisition - EPI_FID_200_fmri"
    assert child.files == []
    assert len(child.packfiles) == 1
    desc = child.packfiles[0]
    assert desc.packfile_type == pvtype
    assert desc.path == "/session/acquisition"
    assert desc.count == 1

    with pytest.raises(StopIteration):
        next(itr)


def make_config(resolver, args=None):
    config = Config(args=args)
    config._resolver = resolver  # pylint: disable=protected-access
    return config


def mock_fs(structure):
    mockfs = fs.open_fs("mem://")

    for path, files in structure.items():
        with mockfs.makedirs(path, recreate=True) as subdir:
            for name in files:
                if isinstance(name, tuple):
                    name, content = name
                else:
                    content = b"Hello World"

                with subdir.open(name, "wb") as f:
                    f.write(content)

    return mockfs
