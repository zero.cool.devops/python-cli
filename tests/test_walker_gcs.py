from unittest import mock

import pytest

from flywheel_cli.walker import GCSWalker

fs_url = "gcs://bucket/path/"


@pytest.fixture
def mocked_tempfile():
    mocked_tempfile_patch = None

    def patcher(return_value=None):
        nonlocal mocked_tempfile_patch
        mocked_tempfile_patch = mock.patch("flywheel_cli.walker.gcs_walker.tempfile")
        mocked_tempfile = mocked_tempfile_patch.start()
        mocked_tempfile.mkdtemp.return_value = return_value
        return mocked_tempfile

    yield patcher

    if mocked_tempfile_patch is not None:
        mocked_tempfile_patch.stop()


@pytest.fixture
def mocked_shutil():
    mocked_shutil_patch = mock.patch("flywheel_cli.walker.gcs_walker.shutil")
    yield mocked_shutil_patch.start()

    mocked_shutil_patch.stop()


@pytest.fixture
def mocked_urlparse():
    mocked_urlparse_patch = None

    def patcher(return_value):
        nonlocal mocked_urlparse_patch
        mocked_urlparse_patch = mock.patch("flywheel_cli.walker.gcs_walker.urlparse")
        mocked_urlparse = mocked_urlparse_patch.start()
        mocked_urlparse.return_value = return_value
        return mocked_urlparse

    yield patcher

    if mocked_urlparse_patch is not None:
        mocked_urlparse_patch.stop()


@pytest.fixture
def mocked_storage():
    mocked_storage = mock.patch("flywheel_cli.walker.gcs_walker.storage")
    yield mocked_storage.start()

    mocked_storage.stop()


# pylint: disable=unused-argument
def test_close_should_request_rmtree_from_shutil_if_tmp_dir_path_exists(
    mocked_storage, mocked_tempfile, mocked_shutil
):
    mocked_tempfile("/tmp")
    walker = GCSWalker(fs_url)

    walker.close()

    mocked_shutil.rmtree.assert_called_with("/tmp")
    assert walker.tmp_dir_path is None


# pylint: disable=unused-argument
def test_close_should_not_request_rmtree_from_shutil_if_tmp_dir_path_does_not_exist(
    mocked_storage, mocked_tempfile, mocked_shutil
):
    mocked_tempfile()
    walker = GCSWalker(fs_url)
    walker.close()

    mocked_shutil.rmtree.assert_not_called()


# pylint: disable=unused-argument
def test_get_fs_url_should_return_fs_url(mocked_storage):
    walker = GCSWalker(fs_url)
    result = walker.get_fs_url()

    assert result == fs_url


def test_init(mocked_storage, mocked_urlparse):
    client_mock = mock.Mock()
    mocked_storage.Client.return_value = client_mock

    mock_urlparse = mocked_urlparse((None, "bucket", "path/"))
    walker = GCSWalker(fs_url)
    mock_urlparse.assert_called_with(fs_url)

    mocked_storage.Client.assert_called_once()
    client_mock.get_bucket.assert_called_once_with("bucket")
    assert walker.client == client_mock

    assert walker.get_fs_url() == fs_url


def test_include_dir(mocker):
    mocker.patch("flywheel_cli.walker.gcs_walker.storage")

    def should_include(dirpath, ignore_dot_files=True, include=None, exclude=None):
        walker = GCSWalker(
            fs_url,
            ignore_dot_files=ignore_dot_files,
            filter_dirs=include,
            exclude_dirs=exclude,
        )
        return walker._include_dir(dirpath)  # pylint: disable=protected-access

    assert not should_include("foo/.bar/baz")
    assert should_include("foo/.bar/baz", ignore_dot_files=False)
    assert not should_include("foo/bar/baz", exclude=["baz"])
    assert not should_include("foo/bar/baz", exclude=["foo"])
    assert should_include("foo/bar/baz", exclude=["foo2"])
    assert not should_include("foo20/bar/baz", exclude=["foo2*"])
