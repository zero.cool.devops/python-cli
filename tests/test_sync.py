# pylint: disable=too-many-lines,protected-access,unused-argument,unused-variable

import argparse
import datetime
import io
import json
import os
import shlex
import tempfile
import zipfile
from contextlib import redirect_stderr
from unittest.mock import ANY, Mock, call, patch

import pytest

from flywheel_cli.commands import sync
from flywheel_cli.sync import fw_threaded_src as fw_src
from flywheel_cli.sync import os_dst, queue, s3_dst


@pytest.fixture(scope="function")
def file_to_dict():
    def file_to_dict_func(file):
        return {
            "size": file.size,
            "modified": file.modified,
            "content": file.read(),
            "is_packed": file.is_packed,
        }

    return file_to_dict_func


@pytest.fixture
def make_fw_mock(tmpdir):
    def make_fw_mock(download_targets_payload=None):
        path = str(tmpdir.join("acq.dicom.zip"))
        member_1 = b"first"
        member_2 = b"second"
        with zipfile.ZipFile(path, "w") as zf:
            zf.writestr("member/path/1.dcm", member_1)
            zf.writestr("member/path/2.dcm", member_2)
        zip_size = os.path.getsize(path)
        zip_mod = float(
            int(os.path.getmtime(path))
        )  # truncate before isoformat does...
        zip_mod_iso = datetime.datetime.fromtimestamp(zip_mod).isoformat()
        zip_content = open(path, "rb").read()  # pylint: disable=consider-using-with

        meta = {"info": {"DicomTag": "Value"}}
        meta_content = json.dumps(meta).encode("utf8")
        meta_size = len(meta_content)

        targets = [
            {
                "container_id": "acq_id",
                "filename": "acq.dicom.zip",
                "filetype": "dicom",
                "download_type": "file",
                "dst_path": "/proj/SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/FILES/acq.dicom.zip",
                "size": zip_size,
                "modified": zip_mod_iso,
            },
            {
                "container_id": "acq_id",
                "filename": "acq.dicom.zip",
                "filetype": None,
                "download_type": "metadata_sidecar",
                "dst_path": "/proj/SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/FILES/acq.dicom.zip.flywheel.json",
                "metadata": meta,
                "size": 0,  # set dynamically in FWFile.__init__ based on json.dumps(metadata)
                "modified": zip_mod_iso,
            },
        ]

        def call_api(url, method, **kwargs):
            url = url.format_map(kwargs.get("path_params"))
            if (method, url) == ("POST", "/download/targets"):
                if download_targets_payload:
                    assert kwargs.get("body") == download_targets_payload
                content = "".join(f"{json.dumps(t)}\r\n" for t in targets)
                return Mock(
                    **{
                        "status_code": 200,
                        "raise_for_status.side_effect": None,
                        "headers": {"content-type": "application/x-ndjson"},
                        "content": content,
                        "iter_lines": lambda: content.strip("\r\n").split("\r\n"),
                    }
                )
            if (method, url) == ("GET", "/containers/acq_id/files/acq.dicom.zip"):
                return Mock(status_code=200, raw=open(path, "rb"))
            assert False, f"Unexpected request {method} {url}"

        def zip_info(container_id, filename):
            assert (container_id, filename) == ("acq_id", "acq.dicom.zip")
            return Mock(
                members=[
                    Mock(path=member.filename, size=member.file_size)
                    for member in zipfile.ZipFile(path).infolist()
                ]
            )

        fw = Mock(
            **{
                "api_client.call_api": call_api,
                "get_container_file_zip_info": zip_info,
            }
        )

        return (
            fw,
            zip_size,
            zip_mod,
            zip_content,
            meta_size,
            meta_content,
            member_1,
            member_2,
        )

    return make_fw_mock


@pytest.fixture()
def fw_mock(make_fw_mock):
    return make_fw_mock()


def test_add_command():
    subparsers = Mock(**{"add_parser.return_value": argparse.ArgumentParser()})
    parser = sync.add_command(subparsers, None)

    def assert_parsed_params(command, **kwargs):
        args = vars(parser.parse_args(shlex.split(command)))
        mock = create_args(return_dict=True, **kwargs)
        for key, value in mock.items():
            assert key in args, f"Expected {key} in args ({sorted(args)})"
            assert args[key] == value, f"Expected {key}={value} (got {args[key]})"

    with pytest.raises(SystemExit), redirect_stderr(io.StringIO()):
        parser.parse_args([])

    assert_parsed_params("src", src="src")

    assert_parsed_params("src dst", src="src", dst="dst")

    assert_parsed_params("src -i t1 --include t2", src="src", include=["t1", "t2"])

    assert_parsed_params("src -e t1 --exclude t2", src="src", exclude=["t1", "t2"])

    assert_parsed_params(
        "src --include-container-tags t1",
        src="src",
        include_container_tags="t1",
    )

    assert_parsed_params(
        "src --exclude-container-tags t1",
        src="src",
        exclude_container_tags="t1",
    )

    with pytest.raises(SystemExit), redirect_stderr(io.StringIO()):
        parser.parse_args(
            shlex.split("src --include-mlset testing --exclude-mlset testing")
        )

    with pytest.raises(SystemExit), redirect_stderr(io.StringIO()):
        parser.parse_args(shlex.split("src --include-mlset asd"))

    assert_parsed_params(
        "src --include-mlset Testing", src="src", include_mlset=["Testing"]
    )
    assert_parsed_params(
        "src --include-mlset Testing --include-mlset training",
        src="src",
        include_mlset=["Testing", "Training"],
    )

    assert_parsed_params(
        "src --exclude-mlset Testing --exclude-mlset training --exclude-mlset vAliDatiON",
        src="src",
        exclude_mlset=["Testing", "Training", "Validation"],
    )

    assert_parsed_params("src -a", src="src", analyses=True)
    assert_parsed_params("src --analyses", src="src", analyses=True)

    assert_parsed_params("src -m", src="src", metadata=True)
    assert_parsed_params("src --metadata", src="src", metadata=True)

    assert_parsed_params("src -x", src="src", full_project=True)
    assert_parsed_params("src --full-project", src="src", full_project=True)

    assert_parsed_params("src -z", src="src", unpack=False)
    assert_parsed_params("src --no-unpack", src="src", unpack=False)

    assert_parsed_params("src -l", src="src", list_only=True)
    assert_parsed_params("src --list-only", src="src", list_only=True)

    assert_parsed_params("src -v", src="src", verbose=True)
    assert_parsed_params("src --verbose", src="src", verbose=True)

    assert_parsed_params("src -n", src="src", dry_run=True)
    assert_parsed_params("src --dry-run", src="src", dry_run=True)

    assert_parsed_params("src -j 2", src="src", jobs=2)
    assert_parsed_params("src --jobs 2", src="src", jobs=2)

    assert_parsed_params("src --delete", src="src", delete=True)

    assert_parsed_params(
        "src --export-templates-file t1",
        src="src",
        export_templates_file="t1",
    )


def test_fw_src(fw_mock, file_to_dict):
    (
        fw,
        zip_size,
        zip_mod,
        zip_content,
        meta_size,
        meta_content,
        _,
        _,
    ) = fw_mock
    src = fw_src.FWThreadedSource(fw, "ticket")
    files = {file.name: file_to_dict(file) for file in src}
    assert files == {
        "proj/SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/FILES/acq.dicom.zip": {
            "size": zip_size,
            "modified": zip_mod,
            "content": zip_content,
            "is_packed": True,
        },
        "proj/SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/FILES/acq.dicom.zip.flywheel.json": {
            "size": meta_size,
            "modified": zip_mod,
            "content": meta_content,
            "is_packed": False,
        },
    }


def test_fw_src_newline(fw_mock, file_to_dict, tmpdir):
    (
        fw,
        zip_size,
        zip_mod,
        zip_content,
        meta_size,
        meta_content,
        _,
        _,
    ) = fw_mock

    path = str(tmpdir.join("acq.dicom.zip"))

    targets = [
        {
            "container_id": "acq_id2",
            "filename": "acq.dicom.zip",
            "filetype": "dicom",
            "download_type": "file",
            "dst_path": "/proj/SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/FILES/acq.dicom.zip",
            "size": zip_size,
            "modified": datetime.datetime.fromtimestamp(zip_mod).isoformat(),
        },
        {
            "container_id": "acq_id2",
            "filename": "acq.dicom.zip",
            "filetype": None,
            "download_type": "metadata_sidecar",
            "dst_path": "/proj/SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/FILES/acq.dicom.zip.flywheel.json",
            "metadata": json.loads(meta_content),
            "size": 0,  # set dynamically in FWFile.__init__ based on json.dumps(metadata)
            "modified": datetime.datetime.fromtimestamp(zip_mod).isoformat(),
        },
    ]

    def call_api(url, method, **kwargs):
        url = url.format_map(kwargs.get("path_params"))
        if (method, url) == ("POST", "/download/targets"):
            content = "".join(f"{json.dumps(t)}\r\n" for t in targets) + "\n \n"
            return Mock(
                **{
                    "status_code": 200,
                    "raise_for_status.side_effect": None,
                    "headers": {"content-type": "application/x-ndjson"},
                    "content": content,
                    "iter_lines": lambda: content.strip("\r\n").split("\r\n"),
                }
            )
        if (method, url) == ("GET", "/containers/acq_id2/files/acq.dicom.zip"):
            return Mock(status_code=200, raw=open(path, "rb"))
        assert False, f"Unexpected request {method} {url}"

    fw.api_client.call_api = call_api

    src = fw_src.FWThreadedSource(fw, "ticket")
    files = {file.name: file_to_dict(file) for file in src}
    assert files == {
        "proj/SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/FILES/acq.dicom.zip": {
            "size": zip_size,
            "modified": zip_mod,
            "content": zip_content,
            "is_packed": True,
        },
        "proj/SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/FILES/acq.dicom.zip.flywheel.json": {
            "size": meta_size,
            "modified": zip_mod,
            "content": meta_content,
            "is_packed": False,
        },
    }


def test_fw_src_include_and_exclude_files_by_tags(make_fw_mock, file_to_dict):
    (fw, zip_size, zip_mod, zip_content, meta_size, meta_content, _, _,) = make_fw_mock(
        download_targets_payload={
            "nodes": [{"level": "project", "_id": "ticket"}],
            "filters": [
                {"types": {"+": "some-tag-to-include"}},
                {"types": {"-": "some-tag-to-exclude"}},
            ],
        }
    )
    src = fw_src.FWThreadedSource(
        fw,
        "ticket",
        include="some-tag-to-include",
        exclude="some-tag-to-exclude",
    )
    files = {file.name: file_to_dict(file) for file in src}
    assert files == {
        "proj/SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/FILES/acq.dicom.zip": {
            "size": zip_size,
            "modified": zip_mod,
            "content": zip_content,
            "is_packed": True,
        },
        "proj/SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/FILES/acq.dicom.zip.flywheel.json": {
            "size": meta_size,
            "modified": zip_mod,
            "content": meta_content,
            "is_packed": False,
        },
    }


def test_fw_src_export_templates(make_fw_mock, file_to_dict):
    (fw, zip_size, zip_mod, zip_content, meta_size, meta_content, _, _,) = make_fw_mock(
        download_targets_payload={
            "nodes": [{"level": "project", "_id": "ticket"}],
            "export_templates": {
                "file": {"key": "template"},
                "hierarchy": {"key": "template"},
            },
        }
    )
    src = fw_src.FWThreadedSource(
        fw,
        "ticket",
        export_templates={
            "file": {"key": "template"},
            "hierarchy": {"key": "template"},
        },
    )
    files = {file.name: file_to_dict(file) for file in src}
    assert files == {
        "proj/SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/FILES/acq.dicom.zip": {
            "size": zip_size,
            "modified": zip_mod,
            "content": zip_content,
            "is_packed": True,
        },
        "proj/SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/FILES/acq.dicom.zip.flywheel.json": {
            "size": meta_size,
            "modified": zip_mod,
            "content": meta_content,
            "is_packed": False,
        },
    }


def test_fw_src_include_and_exclude_container_tags(make_fw_mock, file_to_dict):
    (fw, zip_size, zip_mod, zip_content, meta_size, meta_content, _, _,) = make_fw_mock(
        download_targets_payload={
            "nodes": [{"level": "project", "_id": "ticket"}],
            "container_filters": [
                {"tags": {"plus": ["some-tag-to-include"]}, "type": "subject"},
                {"tags": {"minus": ["some-tag-to-exclude"]}, "type": "session"},
            ],
        }
    )
    src = fw_src.FWThreadedSource(
        fw,
        "ticket",
        include_container_tags={"subject": ["some-tag-to-include"]},
        exclude_container_tags={"session": ["some-tag-to-exclude"]},
    )
    files = {file.name: file_to_dict(file) for file in src}
    assert files == {
        "proj/SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/FILES/acq.dicom.zip": {
            "size": zip_size,
            "modified": zip_mod,
            "content": zip_content,
            "is_packed": True,
        },
        "proj/SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/FILES/acq.dicom.zip.flywheel.json": {
            "size": meta_size,
            "modified": zip_mod,
            "content": meta_content,
            "is_packed": False,
        },
    }


def test_fw_src_include_mlset(make_fw_mock, file_to_dict):
    (fw, zip_size, zip_mod, zip_content, meta_size, meta_content, _, _,) = make_fw_mock(
        download_targets_payload={
            "nodes": [{"level": "project", "_id": "ticket"}],
            "mlset_filters": {"plus": ["Training"]},
        }
    )
    src = fw_src.FWThreadedSource(
        fw,
        "ticket",
        include_mlset=["Training"],
    )
    files = {file.name: file_to_dict(file) for file in src}
    assert files == {
        "proj/SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/FILES/acq.dicom.zip": {
            "size": zip_size,
            "modified": zip_mod,
            "content": zip_content,
            "is_packed": True,
        },
        "proj/SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/FILES/acq.dicom.zip.flywheel.json": {
            "size": meta_size,
            "modified": zip_mod,
            "content": meta_content,
            "is_packed": False,
        },
    }


def test_fw_src_exclude_mlset(make_fw_mock, file_to_dict):
    (fw, zip_size, zip_mod, zip_content, meta_size, meta_content, _, _,) = make_fw_mock(
        download_targets_payload={
            "nodes": [{"level": "project", "_id": "ticket"}],
            "mlset_filters": {"minus": ["Training"]},
        }
    )
    src = fw_src.FWThreadedSource(
        fw,
        "ticket",
        exclude_mlset=["Training"],
    )
    files = {file.name: file_to_dict(file) for file in src}
    assert files == {
        "proj/SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/FILES/acq.dicom.zip": {
            "size": zip_size,
            "modified": zip_mod,
            "content": zip_content,
            "is_packed": True,
        },
        "proj/SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/FILES/acq.dicom.zip.flywheel.json": {
            "size": meta_size,
            "modified": zip_mod,
            "content": meta_content,
            "is_packed": False,
        },
    }


def test_fw_src_strip_root(fw_mock, file_to_dict):
    (
        fw,
        zip_size,
        zip_mod,
        zip_content,
        meta_size,
        meta_content,
        member_1,
        member_2,
    ) = fw_mock
    src = fw_src.FWThreadedSource(fw, "ticket", strip_root=True)
    files = {file.name: file_to_dict(file) for file in src}
    assert files == {
        "SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/FILES/acq.dicom.zip": {
            "size": zip_size,
            "modified": zip_mod,
            "content": zip_content,
            "is_packed": True,
        },
        "SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/FILES/acq.dicom.zip.flywheel.json": {
            "size": meta_size,
            "modified": zip_mod,
            "content": meta_content,
            "is_packed": False,
        },
    }


def test_fw_src_unpack_member(fw_mock):
    (
        fw,
        zip_size,
        zip_mod,
        zip_content,
        meta_size,
        meta_content,
        member_1,
        member_2,
    ) = fw_mock
    items = list(fw_src.FWThreadedSource(fw, "ticket"))

    assert isinstance(items[0], fw_src.FWFile)
    assert items[0].is_packed

    assert isinstance(items[1], fw_src.FWFile)
    assert not items[1].is_packed

    members = items[0].get_members()
    assert len(members) == 2
    member = members[0]
    assert isinstance(member, fw_src.FWMember)

    data = chunk = member.read(1)
    assert chunk
    assert len(chunk) == 1
    while chunk:
        chunk = member.read()
        data += chunk
    assert data == member_1

    temp_path = f"{member.packfile.tempdir}/{member.path}"
    assert os.path.exists(temp_path)
    member.cleanup()
    assert not os.path.exists(temp_path)


def test_fw_src_unpack_member_local(fw_mock):
    (
        fw,
        zip_size,
        zip_mod,
        zip_content,
        meta_size,
        meta_content,
        member_1,
        member_2,
    ) = fw_mock
    items = list(fw_src.FWThreadedSource(fw, "ticket"))

    members = items[0].get_members(True)

    assert len(members) == 2

    contents = set()

    for member in members:
        assert isinstance(member, fw_src.FWMember)
        data = chunk = member.read(1)
        assert chunk
        assert len(chunk) == 1
        while chunk:
            chunk = member.read()
            data += chunk

        contents.add(data)
        temp_path = f"{member.packfile.tempdir}/{member.path}"
        assert os.path.exists(temp_path)
        member.cleanup()
        assert not os.path.exists(temp_path)

    assert set([member_1, member_2]) == contents


def test_os_dest(tmpdir):
    def create_file(name):
        path = str(tmpdir.join(name))
        os.makedirs(os.path.dirname(path), exist_ok=True)
        with open(path, "wt", encoding="utf-8") as file:
            file.write(path)
        stat = os.stat(path)
        return name

    empty_dir = str(tmpdir.join("empty/dir"))
    os.makedirs(empty_dir)
    expected_files = [
        create_file(name)
        for name in (
            "a/b/c",
            "a/b/d",
            "a/e",
            "f",
        )
    ]

    dst = os_dst.OSDestination(tmpdir)
    files = [file.name for file in dst]
    files.sort()
    assert list(files) == expected_files

    file = dst.file("new/file")
    assert file.name == "new/file"
    assert file.filepath == str(tmpdir.join("new/file"))
    assert not os.path.exists(file.filepath)

    file.store(io.BytesIO(b"test"))
    assert os.path.exists(file.filepath)
    with open(file.filepath, encoding="utf-8") as fp:
        assert fp.read() == "test"

    file.delete()
    assert not os.path.exists(file.filepath)

    assert os.path.exists(empty_dir)
    dst.cleanup()
    assert not os.path.exists(empty_dir)


def test_s3_dst():
    boto = Mock(
        **{
            "client.return_value.get_paginator.return_value.paginate.return_value": [
                {
                    "Contents": [
                        {
                            "Key": "prefix/path/1",
                            "Size": 1,
                            "LastModified": datetime.datetime.fromtimestamp(1),
                        },
                    ]
                },
                {
                    "Contents": [
                        {
                            "Key": "prefix/path/2",
                            "Size": 2,
                            "LastModified": datetime.datetime.fromtimestamp(2),
                        },
                    ]
                },
            ]
        }
    )
    patcher = patch("flywheel_cli.sync.s3_dst.boto3", new=boto)
    patcher.start()

    dst = s3_dst.S3Destination("bucket", "prefix/")
    files = [file.name for file in dst]
    assert files == ["path/1", "path/2"]
    boto.client.assert_has_calls(
        [
            call("s3", config=s3_dst.BOTO_CONFIG),
            call().get_paginator("list_objects"),
            call().get_paginator().paginate(Bucket="bucket", Prefix="prefix/"),
        ]
    )

    file = dst.file("path/3")
    assert file.name == "path/3"
    assert file.key == "prefix/path/3"

    boto.reset_mock()
    data = io.BytesIO(b"test")
    file.store(data)
    boto.client().upload_fileobj.assert_called_once_with(
        data, "bucket", "prefix/path/3", Config=s3_dst.S3_TRANSFER_CONFIG
    )

    boto.reset_mock()
    file.delete()
    assert dst.delete_keys == ["prefix/path/3"]
    assert not boto.client().delete_objects.called

    dst.cleanup()
    assert not dst.delete_keys
    boto.client().delete_objects.assert_called_once_with(
        Bucket="bucket", Delete={"Objects": [{"Key": "prefix/path/3"}]}
    )

    boto.reset_mock()
    for i in range(1000):
        file.delete()
    assert not dst.delete_keys
    boto.client().delete_objects.assert_called_once()

    patcher.stop()


@pytest.mark.parametrize(
    "include_container_tags,exclude_container_tags",
    [
        ('{"group": {}}', None),
        (None, '{"group": {}}'),
        ('{"project": {}}', None),
        (None, '{"project": {}}'),
        ('{"subject": {}}', '{"project": {}}'),
    ],
)
def test_sync_with_group_and_project_level_container_filter(
    include_container_tags, exclude_container_tags
):
    args_mock = Mock()
    args_mock.include_container_tags = include_container_tags
    args_mock.exclude_container_tags = exclude_container_tags
    with pytest.raises(ValueError) as exc_info:
        sync.sync(args_mock)

    assert (
        str(exc_info.value)
        == "Group and project level container tag filters are not allowed"
    )


@pytest.mark.parametrize(
    "include_container_tags,exclude_container_tags,analyses_flag",
    [
        ('{"analysis": {}}', None, None),
        (None, '{"analysis": {}}', False),
        ('{"analysis": {}}', '{"subject": {}}', None),
        ('{"subject": {}}', '{"analysis": {}}', False),
    ],
)
def test_sync_with_analysis_level_container_filter_and_analyses_flag_off(
    include_container_tags, exclude_container_tags, analyses_flag
):
    args_mock = Mock()
    args_mock.include_container_tags = include_container_tags
    args_mock.exclude_container_tags = exclude_container_tags
    args_mock.analyses = analyses_flag
    with pytest.raises(ValueError) as exc_info:
        sync.sync(args_mock)

    assert (
        str(exc_info.value)
        == "Analysis container filtering only works with --analyses flag"
    )


def test_sync_with_nonexisiting_export_templates_file(tmpdir):
    test_yaml_file = tmpdir.join("test-yaml")
    args_mock = Mock()
    args_mock.include_container_tags = None
    args_mock.exclude_container_tags = None
    args_mock.export_templates_file = str(test_yaml_file) + "doesntexist"

    with pytest.raises(FileNotFoundError) as exc_info:
        sync.sync(args_mock)

    assert (
        "[Errno 2] No such file or directory: "
        f"'{args_mock.export_templates_file}'" == str(exc_info.value)
    )


def test_queue_wo_unpack(fw_mock):
    (
        fw,
        zip_size,
        zip_mod,
        zip_content,
        meta_size,
        meta_content,
        member_1,
        member_2,
    ) = fw_mock
    items = fw_src.FWThreadedSource(fw, "ticket")

    with tempfile.TemporaryDirectory() as tmpdirname:
        dst = os_dst.OSDestination(tmpdirname)
        syncqueue = queue.SyncQueue(dst, 1)
        for src_file in fw_src.FWThreadedSource(fw, "ticket"):
            syncqueue.store(src_file)
        syncqueue.start()

        syncqueue.wait_for_finish()
        syncqueue.shutdown()

        file_list = fw_src.list_all_files(tmpdirname)
        assert set(file_list) == set(
            [
                "proj/SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/FILES/acq.dicom.zip.flywheel.json",
                "proj/SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/FILES/acq.dicom.zip",
            ]
        )


def test_queue_w_unpack(fw_mock):
    (
        fw,
        zip_size,
        zip_mod,
        zip_content,
        meta_size,
        meta_content,
        member_1,
        member_2,
    ) = fw_mock
    items = fw_src.FWThreadedSource(fw, "ticket")

    with tempfile.TemporaryDirectory() as tmpdirname:
        dst = os_dst.OSDestination(tmpdirname)
        syncqueue = queue.SyncQueue(dst, 1, unpack=True)
        for src_file in fw_src.FWThreadedSource(fw, "ticket"):
            syncqueue.store(src_file)
        syncqueue.start()

        syncqueue.wait_for_finish()
        syncqueue.shutdown()

        file_list = fw_src.list_all_files(tmpdirname)
        assert set(file_list) == set(
            [
                "proj/SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/FILES/acq/1.dcm",
                "proj/SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/FILES/acq/2.dcm",
                "proj/SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/FILES/acq.dicom.zip.flywheel.json",
            ]
        )


def test_sync_audit_log(mocker, fw_mock):
    sdk_mock = mocker.patch.object(
        sync.util, "get_sdk_client_for_current_user"
    ).return_value
    mocker.patch.object(sync.util, "set_sdk_connection_pool_size")
    mocker.patch.object(sync.util, "get_filepath").return_value = "sync_log.csv"

    (
        fw,
        zip_size,
        zip_mod,
        zip_content,
        meta_size,
        meta_content,
        member_1,
        member_2,
    ) = fw_mock

    src = fw_src.FWThreadedSource(fw, "ticket")
    src_mock = mocker.patch.object(sync, "FWThreadedSource").return_value = src

    project_mock = Mock()
    project_mock.id = "proejct_id"
    sdk_mock.lookup.return_value = project_mock

    with tempfile.TemporaryDirectory() as tmpdirname:
        args_mock = Mock()
        args_mock.src = "fw://group/project"
        args_mock.dst = tmpdirname
        args_mock.include_container_tags = None
        args_mock.exclude_container_tags = None
        args_mock.export_templates_file = None
        args_mock.jobs = 1
        args_mock.list_only = False
        args_mock.save_audit_logs = os.path.join(tmpdirname, "report.csv")
        args_mock.no_audit_log = False

        sync.sync(args_mock)

        with open(args_mock.save_audit_logs, "r", encoding="utf-8") as fp:
            log = fp.read()
        log_lines = set(log.splitlines())
        assert log_lines == set(
            [
                "src_path,dest_path,status,existing,error_message,action_taken",
                (
                    "proj/SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/FILES/acq/1.dcm,proj/SUBJECTS/subj/SESSIONS/sess/"
                    "ACQUISITIONS/acq/FILES/acq/1.dcm,completed,False,,Created file on destination"
                ),
                (
                    "proj/SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/FILES/acq/2.dcm,proj/SUBJECTS/subj/SESSIONS/sess/"
                    "ACQUISITIONS/acq/FILES/acq/2.dcm,completed,False,,Created file on destination"
                ),
                (
                    "proj/SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/FILES/acq.dicom.zip.flywheel.json,proj/SUBJECTS/subj/"
                    "SESSIONS/sess/ACQUISITIONS/acq/FILES/acq.dicom.zip.flywheel.json,completed,False,,Created file on destination"
                ),
            ]
        )
        sdk_mock.upload.assert_called_with("project", "proejct_id", "sync_log.csv", ANY)


def test_sync_no_audit_log(mocker, fw_mock):
    sdk_mock = mocker.patch.object(
        sync.util, "get_sdk_client_for_current_user"
    ).return_value
    mocker.patch.object(sync.util, "set_sdk_connection_pool_size")
    mocker.patch.object(sync.util, "get_filepath").return_value = "sync_log.csv"

    (
        fw,
        zip_size,
        zip_mod,
        zip_content,
        meta_size,
        meta_content,
        member_1,
        member_2,
    ) = fw_mock

    src = fw_src.FWThreadedSource(fw, "ticket")
    src_mock = mocker.patch.object(sync, "FWThreadedSource").return_value = src

    project_mock = Mock()
    project_mock.id = "proejct_id"
    sdk_mock.lookup.return_value = project_mock

    with tempfile.TemporaryDirectory() as tmpdirname:
        args_mock = Mock()
        args_mock.src = "fw://group/project"
        args_mock.dst = tmpdirname
        args_mock.include_container_tags = None
        args_mock.exclude_container_tags = None
        args_mock.export_templates_file = None
        args_mock.jobs = 1
        args_mock.list_only = False
        args_mock.save_audit_logs = os.path.join(tmpdirname, "report.csv")
        args_mock.no_audit_log = True

        sync.sync(args_mock)

        with open(args_mock.save_audit_logs, "r", encoding="utf-8") as fp:
            log = fp.read()
        log_lines = set(log.splitlines())
        assert log_lines == set(
            [
                "src_path,dest_path,status,existing,error_message,action_taken",
                (
                    "proj/SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/FILES/acq/1.dcm,proj/SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/"
                    "FILES/acq/1.dcm,completed,False,,Created file on destination"
                ),
                (
                    "proj/SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/FILES/acq/2.dcm,proj/SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/"
                    "FILES/acq/2.dcm,completed,False,,Created file on destination"
                ),
                (
                    "proj/SUBJECTS/subj/SESSIONS/sess/ACQUISITIONS/acq/FILES/acq.dicom.zip.flywheel.json,proj/SUBJECTS/subj/SESSIONS/sess/"
                    "ACQUISITIONS/acq/FILES/acq.dicom.zip.flywheel.json,completed,False,,Created file on destination"
                ),
            ]
        )
        sdk_mock.upload.assert_not_called()


def create_args(return_dict=False, **kwargs):
    """Create mocked parsed CLI args"""
    kwargs.setdefault("src", None)
    kwargs.setdefault("dst", None)
    kwargs.setdefault("include", [])
    kwargs.setdefault("exclude", [])
    kwargs.setdefault("include_container_tags", {})
    kwargs.setdefault("exclude_container_tags", {})
    kwargs.setdefault("analyses", False)
    kwargs.setdefault("metadata", False)
    kwargs.setdefault("full_project", False)
    kwargs.setdefault("unpack", True)
    kwargs.setdefault("list_only", False)
    kwargs.setdefault("verbose", False)
    kwargs.setdefault("dry_run", False)
    kwargs.setdefault("jobs", 4)
    kwargs.setdefault("delete", False)
    kwargs.setdefault("no_audit_log", False)
    if return_dict:
        return kwargs
    return Mock(**kwargs)
