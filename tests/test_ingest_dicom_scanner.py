import collections
from datetime import datetime
from unittest import mock

import pytest
from pydicom.multival import MultiValue

from flywheel_cli import util
from flywheel_cli.ingest import config, errors
from flywheel_cli.ingest import schemas as T
from flywheel_cli.ingest.scanners import dicom
from flywheel_cli.walker import PyFsWalker

from .conftest import AttrDict, DummyWalker


@pytest.fixture(scope="function")
def dummy_scanner():
    scanner = dicom.DicomScanner(
        ingest_config=config.IngestConfig(src_fs="/tmp"),
        strategy_config=None,
        worker_config=config.WorkerConfig(),
        walker=None,
        context={
            "group": {"_id": "group_id"},
            "project": {"label": "project_label"},
        },
    )

    return scanner


@pytest.fixture(scope="function")
def data_for_session(mocker):
    dt = datetime(1900, 1, 2, 3, 4, 5)
    mocker.patch("flywheel_migration.dcm.DicomFile.timestamp", return_value=dt)

    context = {
        "group": {"_id": "group_id"},
        "project": {"label": "project_label"},
        "subject": {"label": "subject_label"},
        "session": {"label": "session_label"},
    }

    dcm = DCMattr_dict(
        {
            "StudyInstanceUID": "1",
            "PatientID": "patient_id",
            "StudyDate": "study_date",
            "StudyTime": "study_time",
            "SeriesDate": "series_date",
            "SeriesTime": "series_time",
            "_manufacturer": "SIEMENS",
        }
    )

    return context, dcm, dt


class DCMattr_dict(AttrDict):
    def get_manufacturer(self):
        return getattr(self, "_manufacturer")


def test_scan(mocker, dummy_scanner):
    dcm = mock.Mock(
        **{
            "get": {
                "StudyInstanceUID": "1",
                "PatientID": "patient_id",
                "StudyDate": "19000102",
                "StudyTime": "030405",
                "SeriesDate": "19010101",
                "SeriesTime": "020304",
                "SeriesInstanceUID": "1.1",
                "AcquisitionNumber": "12",
                "SOPInstanceUID": "1.1.1",
            }.get,
            "get_manufacturer.return_value": "SIEMENS",
            "raw.keys.return_value": ["StudyInstanceUID"],
        }
    )

    mocker.patch("flywheel_cli.ingest.scanners.dicom.DicomFile", return_value=dcm)

    dummy_scanner.context = {
        "group": {"_id": "group_id"},
        "project": {"label": "project_label"},
        "subject": {"label": "subject_label"},
        "session": {"label": "session_label"},
    }
    dummy_scanner.walker = DummyWalker(["file1.dcm"])

    items = list(dummy_scanner.scan("path"))

    assert len(dummy_scanner.sessions) == 1

    assert len(dummy_scanner.sessions["1"].acquisitions) == 1
    assert len(items) == 1
    assert isinstance(items[0], T.ItemWithUIDs)
    item = items[0].item
    assert isinstance(item, T.Item)
    assert item.dir == "path/file1.dcm"
    assert item.context.dict(exclude_none=True) == {
        "group": {"id": "group_id"},
        "project": {"label": "project_label"},
        "subject": {"label": "subject_label"},
        "session": {
            "uid": "1",
            "label": "session_label",
            "timestamp": util.DEFAULT_TZ.localize(datetime(1900, 1, 2, 3, 4, 5)),
            "timezone": str(util.DEFAULT_TZ),
        },
        "acquisition": {
            "uid": "1.1",
            "label": "1901-01-01 02_03_04",
            "timestamp": util.DEFAULT_TZ.localize(datetime(1901, 1, 1, 2, 3, 4)),
            "timezone": str(util.DEFAULT_TZ),
        },
        "packfile": {"type": "dicom", "flatten": True, "zip": True},
    }
    assert len(items[0].uids) == 1
    uid = items[0].uids[0]
    assert isinstance(uid, T.UIDIn)
    assert uid.study_instance_uid == "1"
    assert uid.series_instance_uid == "1.1"
    assert uid.sop_instance_uid == "1.1.1"
    assert uid.acquisition_number == "12"
    assert uid.filename == "path/file1.dcm"
    assert uid.item_id == item.id


def test_scan_invalid_dicom(mocker, dummy_scanner):
    dcm_1 = mock.Mock(
        **{
            "get": {
                "StudyInstanceUID": "1",
                "PatientID": "patient_id",
                "SeriesInstanceUID": "1.1",
                "SOPInstanceUID": "1.1.1",
            }.get,
            "get_manufacturer.return_value": "SIEMENS",
            "raw.keys.return_value": ["StudyInstanceUID"],
        }
    )
    dcm_2 = mock.Mock(
        **{
            "get": {
                "SeriesInstanceUID": "1.1",
                "SOPInstanceUID": "1.1.1",
                "PatientID": "patient_id",
            }.get,
            "raw.keys.return_value": ["StudyInstanceUID"],
        }
    )

    mocker.patch(
        "flywheel_cli.ingest.scanners.dicom.DicomFile", side_effect=[dcm_1, dcm_2]
    )
    dummy_scanner.walker = DummyWalker(["file1.dcm", "file2.dcm"])

    items = list(dummy_scanner.scan("path"))
    assert len(items) == 2
    item = items[0]
    assert isinstance(item, T.ItemWithUIDs)
    assert isinstance(item.item, T.Item)
    assert isinstance(items[1], T.Error)
    error = items[1]
    assert error.code == errors.InvalidDicomFile.code
    assert error.message == "DICOM is missing StudyInstanceUID"
    assert error.filepath == "path/file2.dcm"


def test_scan_dicom_file(mocker, dummy_scanner):
    dcm_mock = mock.Mock(
        **{
            "get": {
                "StudyInstanceUID": "1",
                "PatientID": "patient_id",
                "SeriesDate": "19000102",
                "SeriesTime": "030405",
                "SeriesInstanceUID": "1.1",
                "SOPInstanceUID": "1.1.1",
            }.get,
            "get_manufacturer.return_value": "SIEMENS",
            "raw.keys.return_value": ["StudyInstanceUID"],
        }
    )

    mocker.patch("flywheel_cli.ingest.scanners.dicom.DicomFile", return_value=dcm_mock)

    dummy_scanner.context = {
        "group": {"_id": "group_id"},
        "project": {"label": "project_label"},
        "subject": {"label": "subject_label"},
        "session": {"label": "session_label"},
    }

    dummy_scanner.scan_dicom_file("path/file.dcm", None, [], 123)

    assert len(dummy_scanner.sessions) == 1
    assert len(dummy_scanner.sessions["1"].acquisitions) == 1
    assert dummy_scanner.sessions["1"].acquisitions["1.1"].context == {
        "acquisition": {
            "uid": "1.1",
            "label": "1900-01-02 03:04:05",
            "timestamp": util.DEFAULT_TZ.localize(datetime(1900, 1, 2, 3, 4, 5)),
            "timezone": str(util.DEFAULT_TZ),
        }
    }


def test_resolve_session_without_subject_code_fn(dummy_scanner, data_for_session):

    session = dummy_scanner.resolve_session(
        context=data_for_session[0], dcm=data_for_session[1]
    )

    assert isinstance(session, dicom.DicomSession)
    assert session.context == {
        "session": {
            "uid": "1",
            "label": "session_label",
            "timestamp": data_for_session[2],
            "timezone": str(util.DEFAULT_TZ),
        },
        "subject": {"label": "subject_label"},
    }

    assert session.acquisitions == {}
    assert session.secondary_acquisitions == {}
    assert len(dummy_scanner.sessions) == 1

    re_session = dummy_scanner.resolve_session(
        context=data_for_session[0], dcm=data_for_session[1]
    )
    assert len(dummy_scanner.sessions) == 1
    assert session == re_session


def test_resolve_acquisition_primary_no_related_acquisition(
    dummy_scanner, data_for_session
):
    context = data_for_session[0]
    context["acquisition"] = {"label": "acquisition_label"}

    dcm = data_for_session[1]
    dcm["SeriesInstanceUID"] = "1.1"

    acquisition = dummy_scanner.resolve_acquisition(context=context, dcm=dcm)

    assert isinstance(acquisition, dicom.DicomAcquisition)
    assert acquisition.context == {
        "acquisition": {
            "uid": "1.1",
            "label": "acquisition_label",
            "timestamp": data_for_session[2],
            "timezone": str(util.DEFAULT_TZ),
        }
    }

    assert acquisition.files == {}
    assert acquisition.filenames == {}

    assert len(dummy_scanner.sessions["1"].acquisitions) == 1
    assert len(dummy_scanner.sessions["1"].secondary_acquisitions) == 0
    assert dummy_scanner.sessions["1"].acquisitions["1.1"] == acquisition

    re_acquisition = dummy_scanner.resolve_acquisition(context=context, dcm=dcm)

    assert re_acquisition == acquisition
    assert len(dummy_scanner.sessions["1"].acquisitions) == 1
    assert len(dummy_scanner.sessions["1"].secondary_acquisitions) == 0
    assert dummy_scanner.sessions["1"].acquisitions["1.1"] == re_acquisition


def test_resolve_acquisition_related_acquisition(dummy_scanner, data_for_session):
    context = data_for_session[0]
    context["acquisition"] = {"label": "acquisition_label"}

    dcm = data_for_session[1]
    dcm["SeriesInstanceUID"] = "1.1"
    dcm["ReferencedFrameOfReferenceSequence"] = [
        {
            "RTReferencedStudySequence": [
                {"RTReferencedSeriesSequence": [{"SeriesInstanceUID": "1.1"}]}
            ]
        }
    ]
    dummy_scanner.related_acquisitions = True

    acquisition = dummy_scanner.resolve_acquisition(context=context, dcm=dcm)

    assert isinstance(acquisition, dicom.DicomAcquisition)
    assert acquisition.context == {
        "acquisition": {
            "uid": "1.1",
            "label": "acquisition_label",
            "timestamp": data_for_session[2],
            "timezone": str(util.DEFAULT_TZ),
        }
    }

    assert acquisition.files == {}
    assert acquisition.filenames == {}
    assert len(dummy_scanner.sessions["1"].acquisitions) == 0
    assert len(dummy_scanner.sessions["1"].secondary_acquisitions) == 1
    assert dummy_scanner.sessions["1"].secondary_acquisitions["1.1"] == acquisition


def test_resolve_session_with_subject_code_fn(mocker, attr_dict):
    dt = datetime(1900, 1, 2, 3, 4, 5)
    mocker.patch("flywheel_migration.dcm.DicomFile.timestamp", return_value=dt)

    subject_map = {}

    def subject_fn(fields):
        key = "".join(fields)
        return subject_map.setdefault(key, len(subject_map))

    fn = mock.Mock(side_effect=subject_fn)

    dcm = DCMattr_dict(
        {
            "StudyInstanceUID": "1",
            "PatientID": "patient_id1",
            "PatientName": "patient_name1",
            "StudyDate": "study_date",
            "StudyTime": "study_time",
            "SeriesDate": "series_date",
            "SeriesTime": "series_time",
            "_manufacturer": "SIEMENS",
        }
    )

    scanner = dicom.DicomScanner(
        ingest_config=config.IngestConfig(
            src_fs="/tmp",
            subject_config=config.SubjectConfig(
                code_serial=1,
                code_format="ex{SubjectCode}",
                map_keys=["PatientID", "PatientName"],
            ),
        ),
        strategy_config=None,
        worker_config=config.WorkerConfig(),
        walker=None,
        get_subject_code_fn=fn,
    )

    session = scanner.resolve_session(context=attr_dict({}), dcm=dcm)

    assert session.context == {
        "session": {
            "uid": "1",
            "label": "1900-01-02 03:04:05",
            "timestamp": dt,
            "timezone": str(util.DEFAULT_TZ),
        },
        "subject": {"label": 0},
    }

    assert subject_map["patient_id1patient_name1"] == 0
    fn.assert_called_once_with(["patient_id1", "patient_name1"])


def test_resolve_session_patient_id(mocker, attr_dict):
    dt = datetime(1900, 1, 2, 3, 4, 5)
    mocker.patch("flywheel_migration.dcm.DicomFile.timestamp", return_value=dt)

    dcm = DCMattr_dict(
        {
            "StudyInstanceUID": "1",
            "PatientID": "patient_id1",
            "PatientName": "patient_name1",
            "StudyDate": "study_date",
            "StudyTime": "study_time",
            "SeriesDate": "series_date",
            "SeriesTime": "series_time",
            "_manufacturer": "SIEMENS",
        }
    )

    scanner = dicom.DicomScanner(
        ingest_config=config.IngestConfig(src_fs="/tmp"),
        strategy_config=None,
        worker_config=config.WorkerConfig(),
        walker=None,
    )

    session = scanner.resolve_session(context=attr_dict({}), dcm=dcm)

    assert session.context == {
        "session": {
            "uid": "1",
            "label": "1900-01-02 03:04:05",
            "timestamp": dt,
            "timezone": str(util.DEFAULT_TZ),
        },
        "subject": {"label": "patient_id1"},
    }


def test_scan_dup_sop_uid(mocker, dummy_scanner):
    dcm_1 = mock.Mock(
        **{
            "get": {
                "StudyInstanceUID": "1",
                "PatientID": "patient_id",
                "SeriesInstanceUID": "1.1",
                "SOPInstanceUID": "1.1.1",
            }.get,
            "get_manufacturer.return_value": "SIEMENS",
            "raw.keys.return_value": ["StudyInstanceUID"],
        }
    )
    dcm_2 = mock.Mock(
        **{
            "get": {
                "StudyInstanceUID": "1",
                "PatientID": "patient_id",
                "SeriesInstanceUID": "1.1",
                "SOPInstanceUID": "1.1.1",
            }.get,
            "get_manufacturer.return_value": "SIEMENS",
            "raw.keys.return_value": ["StudyInstanceUID"],
        }
    )

    mocker.patch(
        "flywheel_cli.ingest.scanners.dicom.DicomFile", side_effect=[dcm_1, dcm_2]
    )
    dummy_scanner.walker = DummyWalker(["file1.dcm", "file2.dcm"])

    items = list(dummy_scanner.scan("path"))
    assert len(items) == 1
    item = items[0]
    assert isinstance(item, T.ItemWithUIDs)
    assert isinstance(item.item, T.Item)
    uids = item.uids
    assert len(uids) == 2

    file_paths = []
    for uid in uids:
        assert isinstance(uid, T.UIDIn)
        assert uid.study_instance_uid == "1"
        assert uid.series_instance_uid == "1.1"
        assert uid.sop_instance_uid == "1.1.1"
        file_paths.append(uid.filename)

    assert file_paths == ["path/file1.dcm", "path/file2.dcm"]


def test_scan_invalid_label(mocker, dummy_scanner):
    dcm_1 = mock.Mock(
        **{
            "get": {
                "StudyInstanceUID": "1",
                "SeriesInstanceUID": "1.1",
                "SOPInstanceUID": "1.1.1",
            }.get,
            "raw.keys.return_value": ["StudyInstanceUID"],
        }
    )

    mocker.patch("flywheel_cli.ingest.scanners.dicom.DicomFile", side_effect=[dcm_1])
    dummy_scanner.walker = DummyWalker(["file1.dcm"])

    items = list(dummy_scanner.scan("path"))
    assert len(items) == 1
    assert isinstance(items[0], T.Error)
    error = items[0]
    assert error.code == errors.InvalidDicomFile.code
    assert error.message == "subject.label '' not valid"
    assert error.filepath == "path/file1.dcm"


def test_scan_default_context(mocker, dummy_scanner):
    def filtered_dict(value):
        return {k: v for k, v in dict(value).items() if v is not None}

    dcm = mock.Mock(
        **{
            "get": {
                "StudyInstanceUID": "1",
                "PatientID": "patient_id",
                "SeriesInstanceUID": "1.1",
                "SOPInstanceUID": "1.1.1",
            }.get,
            "get_manufacturer.return_value": "SIEMENS",
            "raw.keys.return_value": ["StudyInstanceUID"],
        }
    )

    mocker.patch("flywheel_cli.ingest.scanners.dicom.DicomFile", side_effect=[dcm])
    dummy_scanner.walker = DummyWalker(["file1.dcm"])
    dt = datetime(1900, 1, 2, 3, 4, 5)
    dummy_scanner.context = {
        **dummy_scanner.context,
        **{
            "session": {"info": {"key": "value"}, "timestamp": dt},
            "acquisition": {"label": "abc"},
        },
    }
    items = list(dummy_scanner.scan("path"))
    item = items[0].item
    assert filtered_dict(item.context.subject) == {"label": "patient_id"}
    assert filtered_dict(item.context.session) == {
        "label": "1",
        "info": {"key": "value"},
        "uid": "1",
        "timestamp": dt,
        "timezone": str(util.DEFAULT_TZ),
    }
    assert filtered_dict(item.context.acquisition) == {
        "label": "abc",
        "uid": "1.1",
        "timezone": str(util.DEFAULT_TZ),
    }


def test_scan_grouping(mocker, dummy_scanner):
    dummy_scanner.ingest_config.zip_single_dicom = True
    non_localizer_dcm1 = mock.Mock(
        **{
            "get": {
                "StudyInstanceUID": "1",
                "PatientID": "patient_id",
                "SeriesInstanceUID": "1.1",
                "SOPInstanceUID": "1.1.1",
                "Modality": "CT",
                "ImageType": ["a", "b", "c"],
            }.get,
            "raw.keys.return_value": ["StudyInstanceUID"],
        }
    )
    localizer_dcm1 = mock.Mock(
        **{
            "get": {
                "StudyInstanceUID": "1",
                "PatientID": "patient_id",
                "SeriesInstanceUID": "1.1",
                "SOPInstanceUID": "1.1.2",
                "Modality": "CT",
                "ImageType": ["a", "b", "c", "localizer"],
            }.get,
            "raw.keys.return_value": ["StudyInstanceUID"],
        }
    )
    non_localizer_dcm2 = mock.Mock(
        **{
            "get": {
                "StudyInstanceUID": "1",
                "PatientID": "patient_id",
                "SeriesInstanceUID": "1.1",
                "SOPInstanceUID": "1.1.3",
                "Modality": "CT",
                "ImageType": "a\\b\\c",
            }.get,
            "raw.keys.return_value": ["StudyInstanceUID"],
        }
    )
    non_localizer_dcm3 = mock.Mock(
        **{
            "get": {
                "StudyInstanceUID": "1",
                "PatientID": "patient_id",
                "SeriesInstanceUID": "1.1",
                "SOPInstanceUID": "1.1.4",
                "Modality": "CT",
                "ImageType": MultiValue(str, ["a", "b", "c"]),
            }.get,
            "raw.keys.return_value": ["StudyInstanceUID"],
        }
    )
    localizer_dcm2 = mock.Mock(
        **{
            "get": {
                "StudyInstanceUID": "1",
                "PatientID": "patient_id",
                "SeriesInstanceUID": "1.1",
                "SOPInstanceUID": "1.1.5",
                "Modality": "CT",
                "ImageType": ["a\\b\\c\\localizer"],
            }.get,
            "raw.keys.return_value": ["StudyInstanceUID"],
        }
    )
    non_localizer_nogroup1 = mock.Mock(
        **{
            "get": {
                "StudyInstanceUID": "1",
                "PatientID": "patient_id",
                "SeriesInstanceUID": "1.1",
                "SOPInstanceUID": "1.1.6",
                "Modality": "CT",
                "ImageType": "a\\b\\x",
            }.get,
            "raw.keys.return_value": ["StudyInstanceUID"],
        }
    )
    non_localizer_nogroup2 = mock.Mock(
        **{
            "get": {
                "StudyInstanceUID": "1",
                "PatientID": "patient_id",
                "SeriesInstanceUID": "1.1",
                "SOPInstanceUID": "1.1.7",
            }.get,
            "raw.keys.return_value": ["StudyInstanceUID"],
        }
    )

    mocker.patch(
        "flywheel_cli.ingest.scanners.dicom.DicomFile",
        side_effect=[
            non_localizer_dcm1,
            localizer_dcm1,
            non_localizer_dcm2,
            non_localizer_dcm3,
            localizer_dcm2,
            non_localizer_nogroup1,
            non_localizer_nogroup2,
        ],
    )
    dummy_scanner.walker = DummyWalker(
        [
            "non_localizer_dcm1.dcm",
            "localizer_dcm1.dcm",
            "non_localizer_dcm2.dcm",
            "non_localizer_dcm3.dcm",
            "localizer_dcm2.dcm",
            "non_localizer_nogroup1.dcm",
            "non_localizer_nogroup2.dcm",
        ]
    )

    items = list(dummy_scanner.scan("path"))
    items = sorted(items, key=lambda x: x.item.filename)

    assert len(items) == 4

    item = items[0].item
    assert item.filename == "1.1.dicom.zip"
    assert item.files_cnt == 3
    assert item.dir == "path"
    assert set(item.files) == set(
        ["non_localizer_dcm1.dcm", "non_localizer_dcm2.dcm", "non_localizer_dcm3.dcm"]
    )

    item = items[1].item
    assert item.filename == "1.1_0.dicom.zip"
    assert item.files_cnt == 1
    assert item.dir == "path/non_localizer_nogroup1.dcm"
    assert set(item.files) == set(["."])

    item = items[2].item
    assert item.filename == "1.1_1.dicom.zip"
    assert item.files_cnt == 1
    assert item.dir == "path/non_localizer_nogroup2.dcm"
    assert set(item.files) == set(["."])

    item = items[3].item
    assert item.filename == "1.1_localizer.dicom.zip"
    assert item.files_cnt == 2
    assert item.dir == "path"
    assert set(item.files) == set(["localizer_dcm2.dcm", "localizer_dcm1.dcm"])


def test_validation_error(mocker, dummy_scanner):
    dcm_1 = mock.Mock(
        **{
            "get": {
                "StudyInstanceUID": "1",
                "PatientID": "",
                "SeriesInstanceUID": "1.1",
                "SOPInstanceUID": "1.1.1",
            }.get,
            "raw.keys.return_value": ["StudyInstanceUID"],
        }
    )

    mocker.patch("flywheel_cli.ingest.scanners.dicom.DicomFile", side_effect=[dcm_1])
    dummy_scanner.walker = DummyWalker(["file1.dcm"])

    with pytest.raises(ValueError) as execinfo:
        list(dummy_scanner.scan("path"))

    assert (
        "_id or label is required field. Use the --subject flag to specify label"
        in str(execinfo.value)
    )
    assert "path/file1.dcm" in str(execinfo.value)


def test_force_scan(temp_fs, dicom_data, dummy_scanner):
    tmpfs, tmpfs_url = temp_fs(
        collections.OrderedDict(
            {
                "DICOM": [
                    (
                        "001",
                        dicom_data(
                            "16844_1_1_dicoms",
                            "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164.dcm",
                        ),
                    ),
                    (
                        "002.txt",
                        dicom_data(
                            "16844_1_1_dicoms",
                            "MR.1.2.840.113619.2.408.5282380.5220731.23348.1516669692.165.dcm",
                        ),
                    ),
                    ("003", b"random file"),
                ],
            }
        )
    )

    dummy_scanner.walker = PyFsWalker(tmpfs_url, src_fs=tmpfs)
    dummy_scanner.ingest_config.force_scan = True
    items = list(dummy_scanner.scan("/"))

    assert len(items) == 2
    assert items[0].item.dir == "DICOM"
    assert len(items[0].uids) == 2
    filenames = [uid.filename for uid in items[0].uids]
    assert set(filenames) == set(["/DICOM/002.txt", "/DICOM/001"])

    assert isinstance(items[1], T.Error)
    error = items[1]
    assert error.code == errors.InvalidDicomFile.code
    assert error.message == "Empty/invalid DICOM file"
    assert error.filepath == "/DICOM/003"


def test_empty_dicom_scan(temp_fs, dummy_scanner):
    tmpfs, tmpfs_url = temp_fs(
        collections.OrderedDict(
            {
                "DICOM": [
                    ("003", b"random file"),
                ],
            }
        )
    )

    dummy_scanner.walker = PyFsWalker(tmpfs_url, src_fs=tmpfs)
    items = list(dummy_scanner.scan("/"))

    assert len(items) == 1
    assert isinstance(items[0], T.Error)
    error = items[0]
    assert error.code == errors.InvalidDicomFile.code
    assert error.message == "Could not parse DICOM file"
    assert error.filepath == "/DICOM/003"
