from datetime import datetime

import pytest

from flywheel_cli.ingest import config, errors
from flywheel_cli.ingest import schemas as s
from flywheel_cli.ingest.scanners import template
from flywheel_cli.walker.fw_walker import FWFileInfo, FWMetaFileInfo

from .conftest import DummyWalker


def test_scan_with_folder_strategy():
    walker = DummyWalker(["group/project/subject/session/file", "random_file"])

    scanner = template.TemplateScanner(
        ingest_config=None,
        strategy_config=config.FolderConfig(),
        worker_config=None,
        walker=walker,
    )

    scan_results = list(scanner.scan("dir"))
    assert len(scan_results) == 2

    # match
    item = scan_results[0]
    assert isinstance(item, s.Item)
    assert item.type == "file"
    assert item.dir == "/group/project/subject/session"
    assert item.files == ["file"]
    assert item.files_cnt == 1
    assert item.bytes_sum == 1
    assert item.context.dict(exclude_none=True) == {
        "group": {"id": "group"},
        "project": {"label": "project"},
        "subject": {"label": "subject"},
        "session": {"label": "session"},
    }

    # not a match
    error = scan_results[1]
    assert isinstance(error, s.Error)
    assert error.filepath == "random_file"
    assert error.code == errors.InvalidFileContext.code
    assert error.message == (
        "Context is invalid for file. Details:\n"
        "2 validation errors for ItemContext\n"
        "group\n"
        "  field required (type=value_error.missing)\n"
        "project\n"
        "  field required (type=value_error.missing)"
    )


def test_scan_with_template_strategy():
    walker = DummyWalker(
        [
            "test_project1/subject1/session1/files/file.dcm",
            "test_project1/subject1/session1/files/file.txt",
            "subject1/file.dcm",
        ]
    )

    scanner = template.TemplateScanner(
        ingest_config=None,
        strategy_config=config.TemplateConfig(
            group="group_id",
            template=[
                {"pattern": "test_{project}"},
                {"pattern": "{subject}"},
                {"pattern": "{session}"},
                {"select": [{"pattern": ".*", "scan": "dicom"}]},
            ],
        ),
        worker_config=None,
        walker=walker,
    )

    scan_results = list(scanner.scan("dir"))

    assert len(scan_results) == 2

    # match, scan task
    task = scan_results[0]
    assert isinstance(task, s.TaskIn)
    assert task.type == "scan"
    assert task.context == {
        "group": {"_id": "group_id"},
        "project": {"label": "project1"},
        "subject": {"label": "subject1"},
        "session": {"label": "session1"},
        "scanner": {
            "type": "dicom",
            "dir": "/test_project1/subject1/session1/files",
            "opts": {},
        },
    }

    # not a match, error
    error = scan_results[1]
    assert isinstance(error, s.Error)
    assert error.filepath == "subject1/file.dcm"
    assert error.code == errors.InvalidFileContext.code
    assert error.message == (
        "Context is invalid for file. Details:\n"
        "1 validation error for ItemContext\n"
        "project\n"
        "  field required (type=value_error.missing)"
    )


@pytest.mark.parametrize(
    "disabled_param,file,pattern_after_project,subject_and_session",
    [
        (
            "no_subjects",
            "test_project1/session/files/file1.dcm",
            "{session}",
            "session",
        ),
        (
            "no_sessions",
            "test_project1/session/files/file1.dcm",
            "{session}",
            "session",
        ),
        (
            "no_subjects",
            "test_project1/subject/files/file1.dcm",
            "{subject}",
            "subject",
        ),
        (
            "no_sessions",
            "test_project1/subject/files/file1.dcm",
            "{subject}",
            "subject",
        ),
    ],
)
def test_scan_with_template_strategy_merge_subject_and_session(
    disabled_param, file, pattern_after_project, subject_and_session
):
    walker = DummyWalker([file])

    config_dict = {
        "group": "group_id",
        "template": [
            {"pattern": "test_{project}"},
            {"pattern": pattern_after_project},
            {"select": [{"pattern": ".*", "scan": "dicom"}]},
        ],
    }
    config_dict[disabled_param] = True
    scanner = template.TemplateScanner(
        ingest_config=None,
        strategy_config=config.TemplateConfig(**config_dict),
        worker_config=None,
        walker=walker,
    )

    scan_results = list(scanner.scan("dir"))

    assert len(scan_results) == 1

    task = scan_results[0]
    assert isinstance(task, s.TaskIn)
    assert task.type == "scan"
    assert task.context == {
        "group": {"_id": "group_id"},
        "project": {"label": "project1"},
        "subject": {"label": subject_and_session},
        "session": {"label": subject_and_session},
        "scanner": {
            "type": "dicom",
            "dir": f"/test_project1/{subject_and_session}/files",
            "opts": {},
        },
    }


@pytest.mark.parametrize(
    "packfile_opts,expected_packfile_name",
    [
        ({"packfile_type": "dicom"}, "session1.dicom.zip"),
        ({"packfile_type": "zip"}, "session1.zip"),
        ({"packfile_type": "zip", "packfile_name": "foo.zip"}, "foo.zip"),
    ],
)
def test_scan_with_template_strategy_w_packfile(packfile_opts, expected_packfile_name):
    walker = DummyWalker(
        [
            "test_project1/subject1/session1/1.dcm",
            "test_project1/subject1/session1/2.dcm",
        ]
    )

    tmpl = [
        {"pattern": "test_{project}"},
        {"pattern": "{subject}"},
    ]
    session_template = {"pattern": "{session}"}
    session_template.update(packfile_opts)
    tmpl.append(session_template)

    scanner = template.TemplateScanner(
        ingest_config=None,
        strategy_config=config.TemplateConfig(
            group="group_id",
            template=tmpl,
        ),
        worker_config=None,
        walker=walker,
    )

    scan_results = list(scanner.scan("dir"))

    assert len(scan_results) == 1

    # match, item with type packfile
    item = scan_results[0]
    assert isinstance(item, s.Item)
    assert item.type == "packfile"
    assert item.dir == "/test_project1/subject1/session1"
    assert item.files == ["1.dcm", "2.dcm"]
    assert item.filename == expected_packfile_name


@pytest.mark.parametrize(
    "packfile_opts,expected_packfile_name,should_zip",
    [
        ({"packfile_type": "dicom"}, "session1.dicom", False),
        ({"packfile_type": "zip"}, "session1.zip", True),
        ({"packfile_type": "zip", "packfile_name": "foo.zip"}, "foo.zip", True),
    ],
)
def test_scan_with_template_strategy_w_packfile_single_file(
    packfile_opts, expected_packfile_name, should_zip
):
    walker = DummyWalker(["test_project1/subject1/session1/1.dcm"])

    tmpl = [
        {"pattern": "test_{project}"},
        {"pattern": "{subject}"},
    ]
    session_template = {"pattern": "{session}"}
    session_template.update(packfile_opts)
    tmpl.append(session_template)

    scanner = template.TemplateScanner(
        ingest_config=config.IngestConfig(
            src_fs="dummy",
        ),
        strategy_config=config.TemplateConfig(
            group="group_id",
            template=tmpl,
        ),
        worker_config=None,
        walker=walker,
    )

    scan_results = list(scanner.scan("dir"))

    assert len(scan_results) == 1

    # match, item with type packfile
    item = scan_results[0]
    assert isinstance(item, s.Item)
    assert item.type == "packfile"
    assert item.dir == "/test_project1/subject1/session1"
    assert item.files == ["1.dcm"]
    assert item.filename == expected_packfile_name
    assert item.context.packfile.zip == should_zip


def test_scan_with_template_strategy_override():
    walker = DummyWalker(
        [
            "project1/subject1/session1/files/file.dcm",
            "project1/subject1/session1/files/file.txt",
            "subject1/file.dcm",
        ]
    )

    scanner = template.TemplateScanner(
        ingest_config=None,
        strategy_config=config.TemplateConfig(
            group="group_id",
            project="project",
            template=[
                {"pattern": "{project}"},
                {"pattern": "{subject}"},
                {"pattern": "{session}"},
                {"select": [{"pattern": ".*", "scan": "dicom"}]},
            ],
            group_override="group_override",
            project_override="project_override",
        ),
        worker_config=None,
        walker=walker,
    )

    scan_results = list(scanner.scan("dir"))

    assert len(scan_results) == 2

    # scan task
    task = scan_results[0]
    assert isinstance(task, s.TaskIn)
    assert task.type == "scan"
    assert task.context == {
        "group": {"_id": "group_override"},
        "project": {"label": "project_override"},
        "subject": {"label": "subject1"},
        "session": {"label": "session1"},
        "scanner": {
            "type": "dicom",
            "dir": "/project1/subject1/session1/files",
            "opts": {},
        },
    }

    # item
    item = scan_results[1]
    assert isinstance(item, s.Item)
    assert item.type == "file"
    assert item.dir == "/subject1"
    assert item.files == ["file.dcm"]
    assert item.files_cnt == 1
    assert item.bytes_sum == 3
    assert item.context.dict(exclude_none=True) == {
        "group": {"id": "group_override"},
        "project": {"label": "project_override"},
    }


def test_scan_with_template_strategy_no_subject():
    walker = DummyWalker(
        [
            "test_project1/session1/files/1.dcm",
        ]
    )

    scanner = template.TemplateScanner(
        ingest_config=None,
        strategy_config=config.TemplateConfig(
            group="group_id",
            template=[
                {"pattern": "test_{project}"},
                {"pattern": "{session}"},
            ],
            no_subjects=True,
        ),
        worker_config=None,
        walker=walker,
    )

    scan_results = list(scanner.scan("dir"))

    assert len(scan_results) == 1

    # match, item
    item = scan_results[0]
    assert isinstance(item, s.Item)
    assert item.type == "file"
    assert item.context.dict(exclude_none=True) == {
        "group": {"id": "group_id"},
        "project": {"label": "project1"},
        "subject": {"label": "session1"},
        "session": {"label": "session1"},
    }


def test_scan_with_dicom_strategy():
    walker = DummyWalker(["subj/session/acq/1.dcm"])

    scanner = template.TemplateScanner(
        ingest_config=None,
        strategy_config=config.DicomConfig(group="grp", project="prj"),
        worker_config=None,
        walker=walker,
    )

    scan_results = list(scanner.scan("foo"))
    assert len(scan_results) == 1

    task = scan_results[0]
    assert isinstance(task, s.TaskIn)
    assert task.type == "scan"
    assert task.context == {
        "group": {"_id": "grp"},
        "project": {"label": "prj"},
        "scanner": {
            "type": "dicom",
            "dir": "foo",
            "opts": {},
        },
    }


def test_scan_with_template_strategy_set_var():
    walker = DummyWalker(
        [
            "test_project1/subject1/session1/acq/file.dcm",
        ]
    )

    scanner = template.TemplateScanner(
        ingest_config=None,
        strategy_config=config.TemplateConfig(
            group="group_id",
            template=[
                {"pattern": "test_{project}"},
                {"pattern": "{subject}"},
                {"pattern": "{session}"},
                {"pattern": ".*"},
            ],
            set_var=["session.label=sessid", "acquisition.label=acquisitionid"],
        ),
        worker_config=None,
        walker=walker,
    )

    scan_results = list(scanner.scan("dir"))
    assert len(scan_results) == 1
    item = scan_results[0]
    assert isinstance(item, s.Item)

    assert item.context.session.label == "session1"
    assert item.context.acquisition.label == "acquisitionid"


def test_scan_with_timestamp():
    walker = DummyWalker(
        [
            "test_project1/subject1/20100110/info/session1/acq/file.dcm",
        ]
    )

    scanner = template.TemplateScanner(
        ingest_config=None,
        strategy_config=config.TemplateConfig(
            group="group_id",
            template=[
                {"pattern": "test_{project}"},
                {"pattern": "{subject}"},
                {"pattern": "{session.timestamp}"},
                {"pattern": "{session.info.abc}"},
                {"pattern": "{session}"},
                {"pattern": ".*"},
            ],
        ),
        worker_config=None,
        walker=walker,
    )

    scan_results = list(scanner.scan("dir"))
    assert len(scan_results) == 1
    item = scan_results[0]
    assert isinstance(item, s.Item)

    assert item.context.session.label == "session1"
    assert item.context.session.info == {"abc": "info"}
    assert item.context.session.timestamp == datetime(2010, 1, 10, 0, 0, 0)


def test_scan_with_folder_strategy_multiple_files():
    walker = DummyWalker(
        [
            "group/project/subject/session/file1",
            "group/project/subject/session/file2",
            "random_file",
        ]
    )

    scanner = template.TemplateScanner(
        ingest_config=None,
        strategy_config=config.FolderConfig(),
        worker_config=None,
        walker=walker,
    )

    scan_results = list(scanner.scan("dir"))
    assert len(scan_results) == 3

    # match
    item = scan_results[0]
    assert isinstance(item, s.Item)
    assert item.type == "file"
    assert item.dir == "/group/project/subject/session"
    assert item.files == ["file1"]
    assert item.files_cnt == 1
    assert item.bytes_sum == 1
    assert item.context.dict(exclude_none=True) == {
        "group": {"id": "group"},
        "project": {"label": "project"},
        "subject": {"label": "subject"},
        "session": {"label": "session"},
    }
    item = scan_results[1]
    assert isinstance(item, s.Item)
    assert item.type == "file"
    assert item.dir == "/group/project/subject/session"
    assert item.files == ["file2"]
    assert item.files_cnt == 1
    assert item.bytes_sum == 2
    assert item.context.dict(exclude_none=True) == {
        "group": {"id": "group"},
        "project": {"label": "project"},
        "subject": {"label": "subject"},
        "session": {"label": "session"},
    }

    # not a match
    error = scan_results[2]
    assert isinstance(error, s.Error)
    assert error.filepath == "random_file"
    assert error.code == errors.InvalidFileContext.code
    assert error.message == (
        "Context is invalid for file. Details:\n"
        "2 validation errors for ItemContext\n"
        "group\n"
        "  field required (type=value_error.missing)\n"
        "project\n"
        "  field required (type=value_error.missing)"
    )


def test_scan_with_project_strategy():
    walker = DummyWalker(
        [
            # every project level file will be skipped
            FWMetaFileInfo(
                "test-project/test-project.flywheel.json",
                1,
                {},
            ),
            # every project level file will be skipped (no enable-project-files)
            FWFileInfo(
                "test-project/FILES/test-skipped-file.dcm",
                1,
                "test-skipped-file.dcm",
                "test-project-container-id",
            ),
            # acq container metadata file
            FWMetaFileInfo(
                "test-project/SUBJECTS/test-subject/SESSIONS/test-session/"
                "ACQUISITIONS/test-acq/test-acq.flywheel.json",
                1,
                {
                    "some": "content",
                    "created": "will-get-trimmed",
                    "id": "will-get-trimmed",
                    "label": "will-get-trimmed",
                    "modified": "will-get-trimmed",
                    "notes": "will-get-trimmed",
                    "permissions": "will-get-trimmed",
                    "revision": "will-get-trimmed",
                },
            ),
            # regular file
            FWFileInfo(
                "test-project/SUBJECTS/test-subject/SESSIONS/test-session/"
                "ACQUISITIONS/test-acq/FILES/test-file.dcm",
                1,
                "test-file.dcm",
                "test-acq-container-id",
            ),
            # regular file's metadata file
            FWMetaFileInfo(
                "test-project/SUBJECTS/test-subject/SESSIONS/test-session/"
                "ACQUISITIONS/test-acq/FILES/test-file.dcm.flywheel.json",
                1,
                {
                    "some": "content",
                    "created": "will-get-trimmed",
                    "hash": "will-get-trimmed",
                    "modified": "will-get-trimmed",
                    "origin": "will-get-trimmed",
                    "path": "will-get-trimmed",
                    "provider_id": "will-get-trimmed",
                    "replaced": "will-get-trimmed",
                    "size": "will-get-trimmed",
                },
            ),
            # regular file without following metadata file
            FWFileInfo(
                "test-project/SUBJECTS/test-subject/SESSIONS/test-session/"
                "ACQUISITIONS/test-acq/FILES/test-file-without-metadata.dcm",
                1,
                "test-file-without-metadata.dcm",
                "test-acq-container-id",
            ),
        ]
    )

    scanner = template.TemplateScanner(
        ingest_config=None,
        strategy_config=config.ProjectConfig(
            src_fs="fw://test-group/test-project",
        ),
        worker_config=None,
        walker=walker,
    )

    scan_results = list(scanner.scan("dir"))
    assert len(scan_results) == 3

    item = scan_results[0]
    assert isinstance(item, s.FWContainerMetadata)
    assert item.path == "test-subject/test-session/test-acq"
    assert item.content == {"some": "content"}

    item = scan_results[1]
    assert isinstance(item, s.Item)
    assert item.type == "file"
    assert item.dir == "test-acq-container-id"
    assert item.files == ["test-file.dcm"]
    assert item.files_cnt == 1
    assert item.bytes_sum == 1
    assert item.context.dict(exclude_none=True) == {
        "group": {"id": "test-group"},
        "project": {"label": "test-project"},
        "subject": {"label": "test-subject"},
        "session": {"label": "test-session"},
        "acquisition": {"label": "test-acq"},
    }
    assert item.fw_metadata == {"some": "content"}

    item = scan_results[2]
    assert isinstance(item, s.Item)
    assert item.type == "file"
    assert item.dir == "test-acq-container-id"
    assert item.files == ["test-file-without-metadata.dcm"]
    assert item.files_cnt == 1
    assert item.bytes_sum == 1
    assert item.context.dict(exclude_none=True) == {
        "group": {"id": "test-group"},
        "project": {"label": "test-project"},
        "subject": {"label": "test-subject"},
        "session": {"label": "test-session"},
        "acquisition": {"label": "test-acq"},
    }


def test_scan_with_project_strategy_with_enabled_project_files():
    walker = DummyWalker(
        [
            # every project level file will be skipped
            FWMetaFileInfo(
                "test-project/test-project.flywheel.json",
                1,
                {},
            ),
            # wont-be-skipped because project files are enabled
            FWFileInfo(
                "test-project/FILES/test-included-file.dcm",
                1,
                "test-included-file.dcm",
                "test-project-container-id",
            ),
        ]
    )

    scanner = template.TemplateScanner(
        ingest_config=config.IngestConfig(
            src_fs="doesn't matter",
            enable_project_files=True,
        ),
        strategy_config=config.ProjectConfig(
            src_fs="fw://test-group/test-project",
        ),
        worker_config=None,
        walker=walker,
    )

    scan_results = list(scanner.scan("dir"))
    assert len(scan_results) == 1

    item = scan_results[0]
    assert isinstance(item, s.Item)
    assert item.type == "file"
    assert item.dir == "test-project-container-id"
    assert item.files == ["test-included-file.dcm"]
    assert item.files_cnt == 1
    assert item.bytes_sum == 1
    assert item.context.dict(exclude_none=True) == {
        "group": {"id": "test-group"},
        "project": {"label": "test-project"},
    }


def test_scan_with_project_strategy_metadata_container_path_sanitization():
    walker = DummyWalker(
        [
            FWMetaFileInfo(
                "test-project/SUBJECTS/test->ubj*ct/SESSIONS/test-session/"
                "ACQUISITIONS/test-acq/test-acq.flywheel.json",
                1,
                {
                    "some": "content",
                    "created": "will-get-trimmed",
                    "id": "will-get-trimmed",
                    "label": "will-get-trimmed",
                    "modified": "will-get-trimmed",
                    "notes": "will-get-trimmed",
                    "permissions": "will-get-trimmed",
                    "revision": "will-get-trimmed",
                },
            ),
        ]
    )

    scanner = template.TemplateScanner(
        ingest_config=None,
        strategy_config=config.ProjectConfig(
            src_fs="fw://test-group/test-project",
        ),
        worker_config=None,
        walker=walker,
    )

    scan_results = list(scanner.scan("dir"))
    assert len(scan_results) == 1

    item = scan_results[0]
    assert isinstance(item, s.FWContainerMetadata)
    assert item.path == "test-_ubj_ct/test-session/test-acq"
    assert item.content == {"some": "content"}


def test_scan_with_project_strategy_no_metadata():
    walker = DummyWalker(
        [
            # every project level file will be skipped
            FWMetaFileInfo(
                "test-project/test-project.flywheel.json",
                1,
                {},
            ),
            # every project level file will be skipped (no enable-project-files)
            FWFileInfo(
                "test-project/FILES/test-skipped-file.dcm",
                1,
                "test-skipped-file.dcm",
                "test-project-container-id",
            ),
            # acq container metadata file
            FWMetaFileInfo(
                "test-project/SUBJECTS/test-subject/SESSIONS/test-session/"
                "ACQUISITIONS/test-acq/test-acq.flywheel.json",
                1,
                {
                    "some": "content",
                    "created": "will-get-trimmed",
                    "id": "will-get-trimmed",
                    "label": "will-get-trimmed",
                    "modified": "will-get-trimmed",
                    "notes": "will-get-trimmed",
                    "permissions": "will-get-trimmed",
                    "revision": "will-get-trimmed",
                },
            ),
            # regular file
            FWFileInfo(
                "test-project/SUBJECTS/test-subject/SESSIONS/test-session/"
                "ACQUISITIONS/test-acq/FILES/test-file.dcm",
                1,
                "test-file.dcm",
                "test-acq-container-id",
            ),
            # regular file's metadata file
            FWMetaFileInfo(
                "test-project/SUBJECTS/test-subject/SESSIONS/test-session/"
                "ACQUISITIONS/test-acq/FILES/test-file.dcm.flywheel.json",
                1,
                {
                    "some": "content",
                    "created": "will-get-trimmed",
                    "hash": "will-get-trimmed",
                    "modified": "will-get-trimmed",
                    "origin": "will-get-trimmed",
                    "path": "will-get-trimmed",
                    "provider_id": "will-get-trimmed",
                    "replaced": "will-get-trimmed",
                    "size": "will-get-trimmed",
                },
            ),
            # regular file without following metadata file
            FWFileInfo(
                "test-project/SUBJECTS/test-subject/SESSIONS/test-session/"
                "ACQUISITIONS/test-acq/FILES/test-file-without-metadata.dcm",
                1,
                "test-file-without-metadata.dcm",
                "test-acq-container-id",
            ),
        ]
    )

    scanner = template.TemplateScanner(
        ingest_config=None,
        strategy_config=config.ProjectConfig(
            src_fs="fw://test-group/test-project", no_metadata=True
        ),
        worker_config=None,
        walker=walker,
    )

    scan_results = list(scanner.scan("dir"))
    assert len(scan_results) == 3

    item = scan_results[0]
    assert isinstance(item, s.FWContainerMetadata)
    assert item.path == "test-subject/test-session/test-acq"
    assert item.content == {}

    item = scan_results[1]
    assert isinstance(item, s.Item)
    assert item.type == "file"
    assert item.dir == "test-acq-container-id"
    assert item.files == ["test-file.dcm"]
    assert item.files_cnt == 1
    assert item.bytes_sum == 1
    assert item.context.dict(exclude_none=True) == {
        "group": {"id": "test-group"},
        "project": {"label": "test-project"},
        "subject": {"label": "test-subject"},
        "session": {"label": "test-session"},
        "acquisition": {"label": "test-acq"},
    }
    assert item.fw_metadata == {}

    item = scan_results[2]
    assert isinstance(item, s.Item)
    assert item.type == "file"
    assert item.dir == "test-acq-container-id"
    assert item.files == ["test-file-without-metadata.dcm"]
    assert item.files_cnt == 1
    assert item.bytes_sum == 1
    assert item.context.dict(exclude_none=True) == {
        "group": {"id": "test-group"},
        "project": {"label": "test-project"},
        "subject": {"label": "test-subject"},
        "session": {"label": "test-session"},
        "acquisition": {"label": "test-acq"},
    }
