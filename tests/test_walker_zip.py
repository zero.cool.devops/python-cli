import os
import shutil
import tempfile

import pydicom

from flywheel_cli.walker import PyFsZipWalker


def test_get_archive_members():
    with tempfile.TemporaryDirectory() as dirname:
        shutil.copyfile(
            os.path.join(os.path.dirname(__file__), "data", "repack_test.zip"),
            os.path.join(dirname, "repack_test.zip"),
        )

        walker = PyFsZipWalker(dirname)
        files = list(walker.list_files())
        assert len(files) == 1
        assert files[0].name == "repack_test.zip"

        assert set(list(walker.get_archive_members(files[0].name))) == set(
            [
                "repack_test.zip/single_dcm.dcm",
                "repack_test.zip/README.md",
                "repack_test.zip/folder1/single_dcm.dcm",
            ]
        )


def test_get_first_dicom():
    with tempfile.TemporaryDirectory() as dirname:
        shutil.copyfile(
            os.path.join(os.path.dirname(__file__), "data", "repack_test.zip"),
            os.path.join(dirname, "repack_test.zip"),
        )

        walker = PyFsZipWalker(dirname)
        list(walker.list_files())
        dicom = walker.open("repack_test.zip")

        ds = pydicom.dcmread(dicom)
        assert (
            ds.get("SOPInstanceUID")
            == "1.2.840.113619.2.408.5282380.5220731.23348.1516669692.164"
        )
