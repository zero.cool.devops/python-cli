from flywheel_cli.importers.bruker_scan import format_timestamp_fn


def test_format_timestamp():
    format_fn = format_timestamp_fn("test.key")
    key, value = format_fn("978611751")
    assert key == "test.key"
    assert value == "2001-01-04T12:35:51Z"

    key, value = format_fn("1495041027")
    assert key == "test.key"
    assert value == "2017-05-17T17:10:27Z"

    assert format_fn("foobar") is None
