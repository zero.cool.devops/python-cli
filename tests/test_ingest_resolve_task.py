import datetime
from unittest import mock
from uuid import uuid4

import flywheel
import pytest

from flywheel_cli.ingest import config, errors
from flywheel_cli.ingest import models as M
from flywheel_cli.ingest import schemas as T
from flywheel_cli.ingest.tasks import resolve


@pytest.fixture(scope="function")
def resolve_task():
    task = T.TaskOut(
        type="resolve",
        id=uuid4(),
        ingest_id=uuid4(),
        status="pending",
        timestamp=0,
        retries=0,
        history=[],
        created=datetime.datetime.now(),
    )
    resolve_task = resolve.ResolveTask(
        db=mock.Mock(
            **{
                "batch_writer_insert_container.return_value.batch_size": 999,
            }
        ),
        task=task,
        worker_config=mock.Mock(),
        is_local=True,
    )
    resolve_task.ingest_config = config.IngestConfig(
        src_fs="/tmp",
    )

    return resolve_task


def test_fw_property(get_sdk_mock):
    db_client_mock = mock.Mock()
    db_client_mock.api_key = "api_key"

    resolve_task = resolve.ResolveTask(
        db=db_client_mock, task=mock.Mock(), worker_config=mock.Mock(), is_local=True
    )
    # make sure __init__ does not get the sdk
    get_sdk_mock.assert_not_called()

    fw = resolve_task.fw
    assert fw == get_sdk_mock.return_value

    get_sdk_mock.assert_called_once_with("api_key")


def test_on_success(resolve_task):
    # pylint: disable=protected-access
    resolve_task._on_success()

    resolve_task.db.set_ingest_status.assert_called_once_with(
        status=T.IngestStatus.in_review
    )
    resolve_task.db.review.assert_not_called()


def test_on_success_assume_yes(resolve_task):
    resolve_task.ingest_config.assume_yes = True

    # pylint: disable=protected-access
    resolve_task._on_success()

    resolve_task.db.set_ingest_status.assert_called_once_with(
        status=T.IngestStatus.in_review
    )
    resolve_task.db.review.assert_called_once()


def test_on_success_detect_duplicates(resolve_task):
    resolve_task.ingest_config.detect_duplicates = True

    # pylint: disable=protected-access
    resolve_task._on_success()

    resolve_task.db.start_detecting_duplicates.assert_called_once()


def test_on_error(resolve_task):
    resolve_task._on_error()  # pylint: disable=protected-access
    resolve_task.db.fail.assert_called_once()
    resolve_task.db.set_ingest_status.assert_not_called()
    resolve_task.db.review.assert_not_called()


def test_resolve_item_containers(sdk_mock, resolve_task):
    sdk_mock.lookup.return_value.to_dict.return_value = {
        "id": "id2",
        "label": "label2",
        "files": [{"name": "name1"}, {"name": "name2"}],
    }
    resp = mock.Mock()
    resp.json.return_value = []
    sdk_mock.api_client.call_api.return_value = resp

    item = T.Item(
        id=uuid4(),
        dir="dir",
        type="packfile",
        filename="test.zip",
        files=["file1", "file2"],
        files_cnt=2,
        bytes_sum=2,
        ingest_id=uuid4(),
        context={
            "group": {"_id": "gid"},
            "project": {"_id": "pid"},
            "session": {"_id": "sid"},
        },
    )

    # pylint: disable=protected-access
    container = resolve_task._resolve_item_containers(item)

    assert resolve_task.visited == {"gid", "gid/<id:pid>"}
    assert isinstance(container, T.Container)


def test_resolve_container_visited(resolve_task):
    resolve_task.visited.add("path")
    # pylint: disable=protected-access
    container = resolve_task._resolve_container(
        c_level=1,
        path=["path"],
        uid_path=[],
        context={"_id": "idval", "label": "labelval"},
        parent=None,
    )

    assert container == resolve_task.db.find_one_container.return_value
    args, _ = resolve_task.db.find_one_container.call_args
    assert len(args) == 1
    condition = args[0]
    assert condition.compare(M.Container.path == "path")


def test_resolve_container_not_visited_no_parent(resolve_task, sdk_mock):
    sdk_mock.lookup.return_value.to_dict.return_value = {
        "id": "grp",
        "label": "grp",
    }
    resolve_task.insert_containers = mock.Mock()

    assert resolve_task.visited == set()

    ctx = T.SourceContainerContext(id="grp_id", label="grp_label")
    # pylint: disable=protected-access
    container = resolve_task._resolve_container(
        c_level=T.ContainerLevel(0), path=["grp"], uid_path=[], context=ctx, parent=None
    )

    assert "grp" in resolve_task.visited
    assert isinstance(container, T.Container)
    assert container.path == "grp"
    assert container.level == 0
    assert container.src_context == ctx
    assert container.dst_context.dict(exclude_none=True) == {
        "id": "grp",
        "label": "grp",
        "files": [],
    }
    resolve_task.insert_containers.push_container.assert_called_once_with(
        "grp", container
    )


def test_resolve_container_not_visited_parent(resolve_task, sdk_mock):
    assert resolve_task.visited == set()

    src_ctx = T.SourceContainerContext(id="prj_id", label="prj")
    parent = T.Container(level=0, path="grp", src_context={"id": "grp"})
    # pylint: disable=protected-access
    container = resolve_task._resolve_container(
        c_level=T.ContainerLevel(1),
        path=["grp", "prj"],
        uid_path=[],
        context=src_ctx,
        parent=parent,
    )

    assert "grp/prj" in resolve_task.visited
    assert isinstance(container, T.Container)
    assert container.path == "grp/prj"
    assert container.level == 1
    assert container.src_context == src_ctx
    assert container.parent_id == parent.id
    # parent does not exist so child also does not exist
    assert container.dst_path is None
    assert container.dst_context is None
    # not call lookup if parent does not exist
    sdk_mock.lookup.assert_not_called()


def test_find_container_in_fw(resolve_task, sdk_mock):
    sdk_mock.lookup.return_value.to_dict.return_value = {
        "id": "bar_id",
        "label": "bar",
        "files": [{"name": "file1"}, {"name": "file2"}],
    }
    # pylint: disable=protected-access
    dst_context = resolve_task._find_container_in_fw(
        ["foo", "bar"],
        T.ContainerLevel.group,
        T.SourceContainerContext(id="bar_id", label="bar"),
    )
    assert dst_context.id == "bar_id"
    assert dst_context.label == "bar"
    assert dst_context.files == ["file1", "file2"]
    sdk_mock.lookup.assert_called_once_with(["foo", "bar"])


def test_run(resolve_task, sdk_mock):
    sdk_mock.lookup.return_value.to_dict.return_value = {
        "id": "bar_id",
        "label": "bar_label",
    }

    resp = mock.Mock()
    resp.json.return_value = []
    sdk_mock.api_client.call_api.return_value = resp
    sdk_mock.get_deid_profile.return_value = None

    item_id = uuid4()
    resolve_task.db.count_all_item.return_value = 1
    resolve_task.db.get_all_item.return_value = [
        T.Item(
            id=item_id,
            dir="dir",
            type="packfile",
            filename="test.zip",
            files=["file1", "file2"],
            files_cnt=2,
            bytes_sum=2,
            ingest_id=uuid4(),
            context={
                "group": {"_id": "grp"},
                "project": {"label": "prj"},
                "subject": {"label": "subject"},
                "packfile": {"type": "zip"},
            },
        )
    ]
    resolve_task.update_items = mock.Mock()
    resolve_task._run()  # pylint: disable=protected-access

    resolve_task.update_items.push.assert_called_once_with(
        {
            "id": item_id,
            "container_id": mock.ANY,
            "existing": False,
        }
    )
    resolve_task.update_items.flush.assert_called_once()


def test_run_skip_project_file(resolve_task, sdk_mock):
    sdk_mock.lookup.return_value.to_dict.return_value = {
        "id": "bar_id",
        "label": "bar_label",
    }

    resp = mock.Mock()
    resp.json.return_value = []
    sdk_mock.api_client.call_api.return_value = resp
    sdk_mock.get_deid_profile.return_value = None

    item_id = uuid4()
    resolve_task.db.count_all_item.return_value = 1
    resolve_task.db.get_all_item.return_value = [
        T.Item(
            id=item_id,
            dir="dir",
            type="packfile",
            filename="test.zip",
            files=["file1", "file2"],
            files_cnt=2,
            bytes_sum=2,
            ingest_id=uuid4(),
            context={
                "group": {"_id": "grp"},
                "project": {"label": "prj"},
                "packfile": {"type": "zip"},
            },
        )
    ]
    resolve_task.update_items = mock.Mock()
    resolve_task._run()  # pylint: disable=protected-access

    resolve_task.update_items.push.assert_called_once_with(
        {
            "id": item_id,
            "container_id": mock.ANY,
            "existing": False,
            "skipped": True,
        }
    )

    resolve_task.insert_errors.push.assert_called_once_with(
        {"item_id": item_id, "code": errors.ProjectFileError.code}
    )

    resolve_task.update_items.flush.assert_called_once()
    resolve_task.insert_errors.flush.assert_called_once()


def test_run_success(resolve_task):
    resolve_task.db.count_all_item.return_value = 0
    resolve_task.db.get_all_item.return_value = []

    resolve_task.run()

    resolve_task.db.update_task.assert_called_once_with(
        resolve_task.task.id, status=T.TaskStatus.completed
    )
    # success
    resolve_task.db.set_ingest_status.assert_called_once_with(
        status=T.IngestStatus.in_review
    )


def test_run_error(resolve_task):
    class FooException(Exception):
        pass

    resolve_task.db.get_all_item.side_effect = FooException()
    resolve_task.run()

    # not set ingest status directly
    resolve_task.db.set_ingest_status.assert_not_called()
    # resolve fails the whole ingest
    resolve_task.db.fail.assert_called_once()


def test_run_require_project(resolve_task, sdk_mock):
    resolve_task.ingest_config.require_project = True
    sdk_mock.lookup.side_effect = flywheel.ApiException()
    _id = uuid4()
    resolve_task.db.count_all_item.return_value = 1
    resolve_task.db.get_all_item.return_value = [
        T.Item(
            id=_id,
            dir="dir",
            type="file",
            filename="file",
            files=["file"],
            files_cnt=2,
            bytes_sum=2,
            ingest_id=uuid4(),
            context={
                "group": {
                    "_id": "gid",
                },
                "project": {"label": "prj"},
            },
        )
    ]
    resolve_task.update_items = mock.Mock()

    with pytest.raises(errors.ContainerDoesNotExist) as exc_info:
        resolve_task._run()  # pylint: disable=protected-access

    assert (
        str(exc_info.value)
        == "The --require-project flag is set and group 'gid' does not exist"
    )


def test_resolve_container_with_fw_metadata(resolve_task):
    resolve_task.strategy_config.strategy_name = "project"
    resolve_task.db.find_one_fw_container_metadata.return_value.content = {
        "operator": "test-operator",
        "not_acceptable": "some-value",
    }
    # pylint: disable=protected-access
    container = resolve_task._resolve_container(
        c_level=T.ContainerLevel.subject,
        # it has to be under project level
        path=["test-group", "test-project", "test-subject"],
        uid_path=[],
        context=T.SourceContainerContext(_id="idval", label="labelval"),
        parent=mock.Mock(spec=T.Container, id=uuid4(), dst_path=None),
    )

    assert container.src_context.operator == "test-operator"
    assert not hasattr(container.src_context, "not_acceptable")


@pytest.mark.parametrize(
    "contexts",
    [
        [
            {"group": {"_id": "gid1"}, "project": {"label": "prj1"}},
            {"group": {"_id": "gid2"}, "project": {"label": "prj2"}},
        ],
        [
            {"group": {"_id": "gid1"}, "project": {"label": "prj1"}},
            {"group": {"_id": "gid1"}, "project": {"label": "prj2"}},
        ],
    ],
)
def test_multiple_groups_or_projects(resolve_task, sdk_mock, contexts):
    sdk_mock.lookup.return_value.to_dict.return_value = {
        "id": "bar_id",
        "label": "bar_label",
    }

    resp = mock.Mock()
    resp.json.return_value = []
    sdk_mock.api_client.call_api.return_value = resp
    sdk_mock.get_deid_profile.return_value = None

    resolve_task.db.count_all_item.return_value = len(contexts)

    items = []
    ingest_id = uuid4()
    for context in contexts:
        item = T.Item(
            id=uuid4(),
            dir="dir",
            type="packfile",
            filename="test.zip",
            files=["file1", "file2"],
            files_cnt=2,
            bytes_sum=2,
            ingest_id=ingest_id,
            context=context,
        )
        items.append(item)

    resolve_task.db.get_all_item.return_value = items

    resolve_task.update_items = mock.Mock()

    with pytest.raises(errors.StopIngestError):
        resolve_task._run()  # pylint: disable=protected-access
        resolve_task.insert_errors.push.assert_called_once_with(
            {"item_id": mock.ANY, "code": errors.MultipleGroupOrProjectError.code}
        )


def test_resolve_item_containers_uid(sdk_mock, resolve_task):
    resolve_task.ingest_config.strict_uid = True
    sdk_mock.lookup.return_value.to_dict.side_effect = [
        {
            "id": "gid",
            "label": "gid",
        },
        {
            "id": "prj_id",
            "label": "pid",
        },
        {
            "id": "subj_id",
            "label": "subj",
        },
    ]

    sdk_mock.get_all_sessions.return_value = [
        flywheel.models.session.Session(
            id="sess_id", label="ses", uid="1.2.3", files=[]
        )
    ]

    sdk_mock.get_all_acquisitions.return_value = [
        flywheel.models.acquisition.Acquisition(
            id="acq_id", label="acq", uid="1.2.3.4", files=[]
        )
    ]

    resp = mock.Mock()
    resp.json.return_value = [1, 2, 3]
    sdk_mock.api_client.call_api.return_value = resp
    sdk_mock.get_deid_profile.return_value = None

    item_id = uuid4()
    resolve_task.db.count_all_item.return_value = 1
    resolve_task.db.get_all_item.return_value = [
        T.Item(
            id=item_id,
            dir="dir",
            type="packfile",
            filename="test.zip",
            files=["file1", "file2"],
            files_cnt=2,
            bytes_sum=2,
            ingest_id=uuid4(),
            context={
                "group": {"_id": "grp"},
                "project": {"label": "prj"},
                "subject": {"label": "subject"},
                "session": {"label": "session", "uid": "1.2.3"},
                "acquisition": {"label": "acquisition", "uid": "1.2.3.4"},
                "packfile": {"type": "zip"},
            },
        )
    ]
    resolve_task.insert_containers = mock.Mock()
    resolve_task._run()  # pylint: disable=protected-access

    args = [
        ("grp", {"id": "gid", "label": "gid"}),
        ("grp/prj", {"id": "prj_id", "label": "pid"}),
        ("grp/prj/subject", {"id": "subj_id", "label": "subj"}),
        ("grp/prj/subject/1.2.3", {"id": "sess_id", "label": "ses", "uid": "1.2.3"}),
        (
            "grp/prj/subject/1.2.3/1.2.3.4",
            {"id": "acq_id", "label": "acq", "uid": "1.2.3.4"},
        ),
    ]

    assert len(args) == len(resolve_task.insert_containers.push_container.mock_calls)
    for index, arg in enumerate(args):
        _, c_args, _ = resolve_task.insert_containers.push_container.mock_calls[index]
        assert c_args[0] == arg[0]
        for key, value in arg[1].items():
            assert getattr(c_args[1].dst_context, key) == value
