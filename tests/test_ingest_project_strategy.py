import pytest

from flywheel_cli.ingest.config import ProjectConfig
from flywheel_cli.ingest.strategies.project import ProjectStrategy


def test_raise_exception_if_no_src_fs_in_config():
    with pytest.raises(ValueError) as execinfo:
        ProjectConfig()

    assert "src_fs can't be None" in str(execinfo.value)


def test_instantiate():
    c = ProjectConfig(src_fs="fw://test-group/test-project")
    s = ProjectStrategy(c)

    assert s.config == c


def test_initial_context():
    c = ProjectConfig(src_fs="fw://test-group/test-project")
    s = ProjectStrategy(c)

    assert s.initial_context() == {
        "group": {"_id": "test-group"},
        "project": {"label": "test-project"},
    }
