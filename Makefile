GOPATH              = $(CURDIR)/go/.gopath
PACKAGE             = flywheel.io/fw
BASE                = $(GOPATH)/src/$(PACKAGE)
SRC                 = $(shell find . -type f -name '*.go' -not -path "./go/vendor/*" -not -path "./go/builds/*" -not -path "./go/python/pkgdata/*")
DIST                = $(CURDIR)/dist
OSARCH             := "darwin/amd64 linux/amd64 windows/amd64"
PLATFORMS          := darwin linux windows
PYTHON_BUILD        = $(CURDIR)/build
PYTHON_DIST         = $(CURDIR)/dist
PYTHON_GO_DIR       = $(CURDIR)/go/python
PYTHON_PKGDATA      = $(PYTHON_GO_DIR)/pkgdata
PYTHON_VENV         = $(CURDIR)/.build_venv
MIN_UPX_VERSION     = 3.96
VERSION            ?= dev
BUILD_HASH         ?= dev
BUILD_DATE         ?= dev
COMMON_LDFLAGS      = -X "main.Version=$(VERSION)" -X "main.BuildHash=$(BUILD_HASH)" -X "main.BuildDate=$(BUILD_DATE)"

.PHONY: all
all: fmt go/vendor | $(PYTHON_PKGDATA) $(PYTHON_VENV) $(BASE)
	cd $(BASE) && GOPATH=${GOPATH} gox -ldflags '$(COMMON_LDFLAGS)' -osarch=$(OSARCH) -output "$(CURDIR)/bin/{{.OS}}_{{.Arch}}/fw"

.PHONY: useMusl
useMusl: all
	cd $(BASE) && CC=`which musl-gcc` GOPATH=${GOPATH} go build -v -ldflags '$(COMMON_LDFLAGS) -linkmode external -extldflags "-static"' -o $(CURDIR)/bin/linux_amd64/fw fw.go

$(PYTHON_VENV): | $(BASE)
	cd $(BASE) && python3.8 -m venv $(PYTHON_VENV)
	( \
		pip install "setuptools<50"; \
		. $(PYTHON_VENV)/bin/activate; \
		pip install pex==2.1.15 poetry==1.1.11; \
		poetry export -f requirements.txt --without-hashes -o install.txt; \
		sed -i -r 's/pywin32==([^;]+);?/pywin32==\1; platform_system == "Windows"/g' install.txt; \
		python $(CURDIR)/compile.py; \
	)

$(BASE):
	@mkdir -p $(dir $@)
	@ln -sf $(CURDIR)/go $@

.PHONY: fmt
fmt: ## Run gofmt on all source files
	$(info running gofmt…)
	@gofmt -l -w $(SRC)

go/glide.lock: go/glide.yaml | $(PYTHON_PKGDATA) $(BASE)
	cd $(BASE) && GOPATH=${GOPATH} glide update
	@test -f $@

$(PYTHON_PKGDATA): | $(PYTHON_VENV) ## Insert packaged data
	$(info running go-bindata...)
	ls $(PYTHON_DIST)
	mkdir -p $(PYTHON_PKGDATA)
	ret=0 && for platform in $(PLATFORMS); do \
	srcdir="$(PYTHON_DIST)/$${platform}-x86_64" ; \
	dstfile="$(PYTHON_PKGDATA)/pkgdata_$${platform}.go" ; \
	go-bindata -nocompress -pkg="pkgdata" -prefix="$$srcdir" -o="$$dstfile" "$$srcdir" || ret="$$?" ; \
	done ; exit "$$ret"

go/vendor: go/glide.lock | $(BASE) ; $(info $(M) retrieving dependencies...)
	$Q cd $(BASE) && GOPATH=${GOPATH} glide --quiet install
	@ln -nsf $(CURDIR)/go $(CURDIR)/go/vendor/src
	@test -d $@

.PHONY: clean
clean: ## Cleanup everything
	$(info cleaning...)
	rm -rf $(PYTHON_BUILD) $(PYTHON_DIST) $(PYTHON_GO_DIR) $(PYTHON_VENV)
	rm -rf bin/ dist/ go/vendor/
	rm -rf $(GOPATH)

.PHONY: fmtCheck
fmtCheck:
	@if [ $(shell gofmt -l $(SRC)) ]; then echo "Format check failed"; exit 1; fi


.PHONY: archive
archive: useMusl compress
	mkdir -p $(DIST)/archives
	cd $(CURDIR)/bin/ && for arch in "darwin_amd64" "windows_amd64" "linux_amd64"; do \
	zip -r "$(DIST)/archives/fw-$${arch}-${VERSION}.zip" "$${arch}" ; \
	cp "$(DIST)/archives/fw-$${arch}-${VERSION}.zip" "$(DIST)/archives/fw-$${arch}.zip" ; \
	done

.PHONY: compress
compress: UPX_VERSION = $(shell upx --version | head -n 1 | cut -f 2 -d ' ')
compress: FLOOR_VERSION = $(shell echo -e "$(MIN_UPX_VERSION)\\n$(UPX_VERSION)" | sort -V | head -n 1)
compress:
	if [ "$(MIN_UPX_VERSION)" != "$(UPX_VERSION)" ]; then echo "UPX version is too old"; exit 1; fi
	cd $(CURDIR)/bin/ && for bin in "darwin_amd64/fw" "windows_amd64/fw.exe" "linux_amd64/fw"; do \
	upx "$${bin}" ; \
	done
