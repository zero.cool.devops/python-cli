package command

import (
	. "fmt"

	"github.com/spf13/cobra"

	"flywheel.io/public/sdk/api"

	"flywheel.io/fw/ops"
	"flywheel.io/fw/util"
)

var UserAgent = ""

// Each command is separated into its own function.
// This is to prevent any flag variable pointers from cross-contaminating.

func BuildCommand(version, buildHash, buildDate string) *cobra.Command {
	// Set UserAgent string based on provided version
	UserAgent = Sprintf("CLI/%s", version)

	o := opts{}

	cmd := o.fw()
	cmd.AddCommand(o.batch())
	cmd.AddCommand(o.gear())
	cmd.AddCommand(o.job())
	cmd.AddCommand(o.bidsCommand())

	AddDelegateCommand(cmd, "login", "Login to a Flywheel instance")
	AddDelegateCommand(cmd, "logout", "Delete your saved API key")
	AddDelegateCommand(cmd, "ls", "Show remote files")
	AddDelegateCommand(cmd, "cp", "Copy a local file to a remote location, or vice-a-versa")
	AddDelegateCommand(cmd, "import", "Import data into Flywheel")
	AddDelegateCommand(cmd, "export", "Export data from Flywheel")
	AddDelegateCommand(cmd, "download", "Download a remote file or container")
	AddDelegateCommand(cmd, "upload", "Upload a local file to a Flywheel container")
	AddDelegateCommand(cmd, "sync", "Sync files from Flywheel to storage")
	AddDelegateCommand(cmd, "ingest", "Ingest large datasets into Flywheel")
	AddDelegateCommand(cmd, "deid", "Create and test de-identification template")
	AddDelegateCommand(cmd, "status", "See your current login status")
	AddDelegateCommand(cmd, "version", "Print CLI version")

	pythonCmd := AddDelegateCommand(cmd, "python", "Execute python process")
	pythonCmd.Hidden = true

	adminCmd := AddDelegateCommand(cmd, "admin", "Site administration commands")
	adminCmd.Hidden = true

	util.VersionString = "Flywheel CLI " + version + " build " + buildHash + " on " + buildDate

	return cmd
}

type opts struct {
	Client      *api.Client
	Credentials *Creds
}

func (o *opts) fw() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "fw",
		Short: "Flywheel command-line interface",

		PersistentPreRun: func(cmd *cobra.Command, args []string) {
			o.initClient()
		},
	}

	return cmd
}

func (o *opts) version(version, buildHash, buildDate string) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "version",
		Short: "Print CLI version",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			ops.Version(version, buildHash, buildDate)
		},
	}

	return cmd
}

// General client initialization. Calling once already initialized is a no-op.
func (o *opts) initClient() {
	if o.Credentials == nil {
		o.Credentials, _ = LoadCreds()
	}
	if o.Client == nil && o.Credentials != nil {
		o.Client, _ = MakeClientWithCreds(o.Credentials.Key, o.Credentials.Insecure)
	}
}

// Helper func that requires a valid API key on disk
func (o *opts) RequireClient(cmd *cobra.Command, args []string) {

	// If you use RequireClient as a PersistentPreRun on a subcommand, it
	// will obliterate the root command's closure. For this reason, duplicate
	// what it does here!
	o.initClient()

	if o.Client == nil {
		Println("You are not currently logged in.")
		Println("Try `fw login` to login to Flywheel.")
		util.Fatal(1)
	}
}
