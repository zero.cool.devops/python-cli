#!/usr/bin/env python3
"""Cross-platform (linux, darwin, windows) build CLI using PEX"""

import datetime as dt
import json
import logging
import os
import py_compile
import re
import shutil
import sys
import tarfile
import urllib.request
import zipfile

from pex import pex_builder, resolver

logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.StreamHandler(sys.stdout)],
)
LOG = logging.getLogger()

SRC_DIR = os.path.abspath(os.path.dirname(__file__))
BUILD_DIR = os.path.join(SRC_DIR, "build")
DIST_DIR = os.path.join(SRC_DIR, "dist")
PEX_BUILD_CACHE_DIR = os.path.join(BUILD_DIR, "pex-cache")
PACKAGE_CACHE_DIR = os.path.join(BUILD_DIR, "src-packages")
REQUIREMENTS_FILE = os.path.join(SRC_DIR, "install.txt")
PYTHON_VERSION = "3.8.5"
PYTHON_DIST_URL = "https://storage.googleapis.com/flywheel-dist/dependencies"
PYTHON_PACKAGES = [
    (
        "darwin-x86_64",
        "{0}/python-{1}/python-{1}_darwin-x86_64.tar.gz".format(
            PYTHON_DIST_URL, PYTHON_VERSION
        ),
    ),
    (
        "linux-x86_64",
        "{0}/python-{1}/python-{1}_linux-x86_64.tar.gz".format(
            PYTHON_DIST_URL, PYTHON_VERSION
        ),
    ),
    (
        "windows-x86_64",
        "{0}/python-{1}/python-{1}-embed-amd64.zip".format(
            PYTHON_DIST_URL, PYTHON_VERSION
        ),
    ),
]
PYVER = ".".join(PYTHON_VERSION.split(".")[:2])  # Short version
PYXY = "".join(PYTHON_VERSION.split(".")[:2])


def read_ignore_patterns():
    """
    Read package-ignore file and return with a list of patterns
    that specifies which files should be ignored.
    """
    result = []
    pkg_ignore_path = os.path.join(SRC_DIR, "package-ignore.txt")
    with open(pkg_ignore_path, "r") as f:
        for line in f.readlines():
            line = line.strip()
            if line:
                result.append(re.compile(line))
    return result


def ignore_file(filename, patterns):
    """
    Determine that the given file should be ignored or not
    according to the given patterns.
    """
    for pattern in patterns:
        if pattern.search(filename):
            return True

    return False


def update_pth_file(path):
    """Update _pth file with PYVER"""
    if os.path.isfile(path):
        with open(path, "a") as f:
            f.write(f"\r\nlib/python{PYVER}/site-packages\r\n")


def build_site_packages():
    """Build site packages"""
    # Read ignore list
    # See package-ignore.txt, largely we're removing test files and
    # Multi-megabyte dicoms from the dicom folder
    ignore_patterns = read_ignore_patterns()

    LOG.info("Resolving distributions...")

    resolved_distributions = resolver.resolve(
        requirements=[SRC_DIR],
        requirement_files=[REQUIREMENTS_FILE],
        cache=PEX_BUILD_CACHE_DIR,
    )
    builder = pex_builder.PEXBuilder()

    for resolved_dist in resolved_distributions:
        LOG.info(resolved_dist.distribution)
        builder.add_distribution(resolved_dist.distribution)
        builder.add_requirement(resolved_dist.requirement)

    # After this point, builder.chroot contains a full list of the files
    LOG.info("Compiling package")
    builder.freeze(bytecode_compile=False)

    LOG.info("Creating site-packages.zip")
    site_packages_path = os.path.join(BUILD_DIR, "site-packages.zip")

    # Create an uncompressed site-packages.zip and add all of the discovered files
    # (Except those that are filtered out)
    with open(site_packages_path, "wb") as f:
        added_files = set()
        with zipfile.ZipFile(f, "w") as zf:
            for filename in sorted(builder.chroot().files()):
                if ignore_file(filename, ignore_patterns):
                    LOG.debug(f"ignore: {filename}")
                    continue

                if not filename.startswith(".deps"):
                    continue

                # Determine new path
                src_path = os.path.join(builder.chroot().chroot, filename)
                dst_path = "/".join(filename.split("/")[2:])

                # Optionally, compile the file
                _, ext = os.path.splitext(src_path)
                if ext == ".py":
                    cfile_path = src_path + "c"
                    dst_path += "c"

                    LOG.debug(f"compiling: {dst_path}")
                    py_compile.compile(
                        src_path, cfile=cfile_path, dfile=dst_path, optimize=1
                    )
                    src_path = cfile_path

                if not dst_path in added_files:
                    LOG.debug(f"add: {dst_path}")
                    zf.write(src_path, dst_path)
                    added_files.add(dst_path)

    return site_packages_path


def main():
    """Build site-packages, download platform specific python interpreters and zip them together"""
    site_packages_path = build_site_packages()

    if os.path.isdir(DIST_DIR):
        shutil.rmtree(DIST_DIR)

    # Download and extract a python interpreter for each platform under build/{dist_name}
    for dist_name, package_url in PYTHON_PACKAGES:
        LOG.info(f"Building {dist_name}")

        # Determine the name of the package file
        package_name = os.path.basename(package_url).replace(".tar.gz", ".tgz")
        package_path = os.path.join(PACKAGE_CACHE_DIR, package_name)

        # Only perform this step if the interpreter folder doesn't already exist
        python_dist_dir = os.path.join(BUILD_DIR, dist_name)
        if not os.path.isdir(python_dist_dir):
            # Download the python interpreter if not cached
            if not os.path.isfile(package_path):
                if not os.path.isdir(PACKAGE_CACHE_DIR):
                    os.makedirs(PACKAGE_CACHE_DIR)

                with urllib.request.urlopen(package_url) as response, open(
                    package_path, "wb"
                ) as out:
                    shutil.copyfileobj(response, out)

            # Extract to dist folder
            os.makedirs(python_dist_dir)

            _, ext = os.path.splitext(package_name)
            if ext == ".tgz":
                with tarfile.open(package_path, "r") as tf:
                    tf.extractall(python_dist_dir)
            else:
                with open(package_path, "rb") as f:
                    with zipfile.ZipFile(f, "r") as zf:
                        zf.extractall(python_dist_dir)

            # Update windows python{}._pth file:
            # See: https://docs.python.org/3/using/windows.html#finding-modules
            # Effectively we need to re-add the site-packages folder to the overridden path file
            pth_file = os.path.join(python_dist_dir, "python{}._pth".format(PYXY))
            update_pth_file(pth_file)

        # Zip results (with no compression, since we'll use upx on the binary)
        dist_dir = os.path.join(DIST_DIR, dist_name)
        os.makedirs(dist_dir)

        # Re-zip everything to an uncompressed zipfile
        dst_archive_path = os.path.join(
            dist_dir, "python-{}.zip".format(PYTHON_VERSION)
        )
        with open(dst_archive_path, "wb") as f:
            with zipfile.ZipFile(f, "w") as zf:
                # Copy dist-dir contents
                for root, _, files in os.walk(python_dist_dir):
                    for filename in files:
                        src_path = os.path.join(root, filename)
                        arc_path = os.path.relpath(src_path, python_dist_dir)
                        zf.write(src_path, arc_path)

        # Copy site-packages
        shutil.copyfile(site_packages_path, os.path.join(dist_dir, "site-packages.zip"))

        # Write build info, which will be used in go to determine whether or not to extract
        # the interpreter and/or the site-packages.zip
        ver_info_path = os.path.join(dist_dir, "version.json")
        with open(ver_info_path, "w") as f:
            json.dump(
                {
                    "python_version": PYTHON_VERSION,
                    "py_ver": PYVER,
                    "build_time": dt.datetime.utcnow().isoformat(),
                },
                f,
            )


if __name__ == "__main__":
    main()
